/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    "text!sga1/template/schedule.html"
], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template : _.template(template),

        initialize: function () {
            _.bindAll(this, 'onRender');
        },
        onRender: function () {

        }

    });

});