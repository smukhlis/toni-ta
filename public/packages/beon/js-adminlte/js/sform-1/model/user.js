define([
    'backbone'
], function(Backbone) {
    return Backbone.Model.extend({
        urlRoot: BASE_URL + '/dashboard/editor-jh/logos'
    });

});

