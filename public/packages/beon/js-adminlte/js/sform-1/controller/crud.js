
define([
    'jquery',
    'sform1/view/textarea',
    'sform1/collection/coll',
    'sform1/view/panel',
    'sform1/view/test-table',
    'sform1/view/pagination-coll',
    'sform1/view/box-table',
    'sform1/view/form-layout-1'
], function ($, Textarea,User, Panel, Tbl, Pagination, Box, Form) {

    var body = $('.content-box');

    var box = new Box({title:'Data '+APP.nameList});

    var panel = new Panel({title:'Title Panel'});

    //var text = new Textarea({label:'Contoh Textarea'});

    //var form = new Form({title:'Add Customer'});


    //form.render();




    panel.render();
    box.render();
    //box.ui.body.prepend(form.el);
    body.append(box.el);

    var user = new User(APP.pageEnd);

    console.log(user);

    user.state.totalRecords = APP.count;
    if(user.state.totalRecords==0)user.state.totalRecords=1;
    //user.queryParams.totalRecords = APP.count;

    var tbl = new Tbl({collection: user});
    tbl.render();
    box.appendTable(tbl);

    var pag = new Pagination();
    pag.render();

    pag.setPaginator(user);

    pag.setPageCurrent(pag.pageLast-1, true);

    box.ui.body.append(pag.el);



    //$('.c-body').append(text.render().el);

    //panel.ui.cBody.append(text.render().el);
    //console.log(panel.ui.cBody.attr('class'));



//    $.when(user.fetch())
//        .done(function(){
//
//            console.log(user);
//
//
//
//
//
//            //user.save();
//
//        });






//    console.log(user);
//    user.pager().done(function (resp, status, xhr) {
//        user.afterDone(xhr);
//        console.log(user);
//    });

});
