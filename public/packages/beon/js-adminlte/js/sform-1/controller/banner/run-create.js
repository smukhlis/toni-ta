define([
    'jquery',
    'app/controller/banner/create',
    'jqueryUI', 'elfinder', 'daterangepicker'
], function($, Test) {

    var test = new Test();
    console.log(test);
    $("#xxx").html(test.render().el);


    $('#time').daterangepicker({ timePicker: true, timePickerIncrement: 7, format: 'DD-MM-YYYY hh:mm:ss' });


    $(document).on('submit', '#create-form', function()
    {
        $('.help-error').remove();
        $('.has-error').removeClass('has-error');
        var sArray = $(this).serializeArray();
        sArray.push({"name":"data", "value":test.cm.getValue()});
        console.log(sArray);
        $.ajax({
            "type": "POST",
            "url": window.location.href.toString().replace('/create',''),
            "data": sArray,
            "dataType": "json"
        }).done(function(result)
        {
            console.log(result);
            if(result.customerCreated === false)
            {
                if(typeof result.message !== 'undefined')
                {
                    showStatusMessage(result.message, result.messageType);
                }
                else if(typeof result.errorMessages !== 'undefined')
                {
                    showRegisterFormAjaxErrors(result.errorMessages);
                }
            }
            else
            {
                window.location = result.redirectUrl;
            }
        }).fail(function (jqXHR) {
            console.log(jqXHR.responseJSON);
            for(var key in jqXHR.responseJSON){
                console.log(key);
                var field = $('#'+key);
                field.after('<span class="help-block help-error">'+jqXHR.responseJSON[key][0]+'</span>');
                field.parent().parent().addClass('has-error');
            }
        });

        return false;
    });


    var dialogChangeImage=undefined;

    var changeImage = function(){
        if(!dialogChangeImage){
            dialogChangeImage = $("<div/>").dialogelfinder({
                url: BASE_URL + '/elfinder/connector',
                commandsOptions: {
                    getfile: {
                        multiple : false,
                        // allow to return filers info
                        folders  : false,
                        oncomplete: "close" // close/hide elFinder
                    }
                },
                width: $(window).width() - 20,
                height: $(window).height() - 150,
                getFileCallback: function (file) {
                    $('#path').val(file.url.replace(file.baseUrl, ''));
                    $('.preview').attr('src', file.url);
                    test.changeBackground(file.url);
                    //console.log('file');
                    //console.log(file);
                    //t.trigger("file:selected", file);
                }
            }).css("top", 56);
            //dialogChangeImage.dialogelfinder("open");
        } else {
            // reopen elFinder
            dialogChangeImage.dialogelfinder("open");
        }
    };

    var preview = $('.preview');
    var path = $('#path');

    path.change(function(){
        preview.attr('src', BASE_URL + '/assets/images/' + path.val() );
    });

    $('.change-image').click(changeImage);
    preview.click(changeImage);

});