/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    'sform1/view/pagination-li'
], function($, _, Marionette, Li) {

    return Marionette.ItemView.extend({
        defaults: {
            countLi: 5
        },
        template : _.template('<li class="first disabled"><a class="first" href="#">First</a></li><li class="prev disabled"><a href="#">Previous</a></li><li class="active"><a href="#">1</a></li><li class="next disabled"><a href="#">Next</a></li><li class="last disabled"><a href="#">Last</a></li>'),
        className : 'pagination pull-right',
        tagName : 'ul',

        events: {
            'click .first a':'goToFirstPage',
            'click .last a':'goToLastPage',
            'click .next a':'goToNextPage',
            'click .prev a':'goToPrevPage'
        },

        ui:{
            liFirst:'.first',
            liPrev:'.prev',
            liCurrent:'.active',
            liNext:'.next',
            liLast:'.last',
            aFirst:'.first a',
            aPrev:'.prev a',
            aCurrent:'.active a',
            aNext:'.next a',
            aLast:'.last a'
        },

        pageSize:20,
        pageTotal:1,
        pageCurrent:1,
        pageFirst:1,
        pageLast:1,
        pagePrev:1,
        //pageShow:10,
        itemTotal:0,

        childLiNext:[],
        childLiPrev:[],

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender', 'setTotalRecords', 'setPageTotal', 'setDisable', 'setActive', 'removeChildLi',
                'setPageCurrent', 'buildChildLi', 'goToPage', 'goToNextPage', 'goToPrevPage', 'goToFirstPage', 'goToLastPage');

            for (i = 0; i < this.options.countLi; i++) {
                var li = new Li({ pagination:this});
                li.render();
                this.childLiNext.push(li);
                var li2 = new Li({ pagination:this});
                li2.render();
                this.childLiPrev.push(li2);
            }
        },
        onRender: function () {

        },

        goToPage: function(number){
            this.setPageCurrent(number);
        },

        goToFirstPage: function(e){
            if(e)e.preventDefault();
            if(this.pageCurrent!=this.pageFirst)this.setPageCurrent(this.pageFirst);
            return false;
        },

        goToLastPage: function(e){
            if(e)e.preventDefault();
            if(this.pageCurrent!=this.pageLast)this.setPageCurrent(this.pageLast);
            return false;
        },

        goToNextPage: function(e){
            if(e)e.preventDefault();
            if(this.pageCurrent+1<=this.pageLast)this.setPageCurrent(this.pageCurrent+1);
            return false;
        },

        goToPrevPage: function(e){
            if(e)e.preventDefault();
            if(this.pageCurrent-1>=this.pageFirst)this.setPageCurrent(this.pageCurrent-1);
            return false;
        },

        setTotalRecords: function (total) {
            console.log(total);
            console.log('ppoppo');
            this.setPageTotal(Math.ceil(total / this.pageSize));
        },

        setPageCurrent: function(number){
            this.pageCurrent = number;
            this.removeChildLi();
            this.buildChildLi();
            this.ui.aCurrent.html(number);
        },

        removeChildLi: function(){
            var t = this;
            _.each(this.childLiNext, function(e){
                e.$el.hide();
            });

            _.each(this.childLiPrev, function(e, i){
                e.$el.hide();
            });
        },

        setPageTotal: function (total) {
            console.log('total');
            console.log(total);
            this.pageTotal = total;
            this.pageLast = total+this.pageFirst;

            console.log(this.pageLast);

            // bila total satu hlaman saja
            if(total==this.pageFirst){
                this.setDisable()
            }else{
                this.setActive();
            }



            this.removeChildLi();
            this.buildChildLi();

//            _.each(this.childLiNext, function(e, i){
//                e.close();
//                e.splice(i);
//            });
//
//            _.each(this.childLiPrev, function(e, i){
//                e.close();
//                e.splice(i);
//            });

//            var halfPageShow = Math.floor(this.pageShow / 2);
//
//
//            for (i = 0; i < halfPageShow; i++) {
//                var number = this.pageCurrent-(i+1);
//                if(number >= this.pageFirst){
//                    var li = new Li({number:number});
//                    this.ui.liCurrent.before(li.render().el);
//                }
//
//            }
//
//            for (i = halfPageShow; i > 0; i--) {
//                var number = this.pageCurrent+(i);
//                console.log(number);
//                if(number <= this.pageLast){
//                    var li = new Li({pageNumber:number, pagination:this});
//                    console.log(li);
//                    this.ui.liCurrent.after(li.render().el);
//                }
//
//            }


        },

        buildChildLi: function(){

            if(this.pageCurrent==this.pageFirst){
                this.ui.liFirst.addClass('disabled');
                this.ui.liPrev.addClass('disabled');
            }else{
                this.ui.liFirst.removeClass('disabled');
                this.ui.liPrev.removeClass('disabled');
            }


            if(this.pageCurrent<this.pageLast-1){
                this.ui.liLast.removeClass('disabled');
                this.ui.liNext.removeClass('disabled');
            }else{
                this.ui.liLast.addClass('disabled');
                this.ui.liNext.addClass('disabled');
            }


            for (i = 0; i < this.options.countLi; i++) {
                var number = this.pageCurrent-this.options.countLi+i;
                if(number >= this.pageFirst){
                    this.childLiPrev[i].options.pageNumber=number;
                    this.childLiPrev[i].ui.a.html(number);
                    this.ui.liCurrent.before(this.childLiPrev[i].el);
                    this.childLiPrev[i].$el.css('display', 'inline');
                }

            }

            for (i = this.options.countLi; i > 0; i--) {
                var number = this.pageCurrent+(i);
                if(number < this.pageLast){
                    this.childLiNext[i-1].options.pageNumber=number;
                    this.childLiNext[i-1].ui.a.html(number);
                    this.ui.liCurrent.after(this.childLiNext[i-1].el);
                    this.childLiNext[i-1].$el.css('display', 'inline');
                }

            }
        },

        setDisable: function(){
            this.ui.liFirst.addClass('disabled');
            this.ui.liLast.addClass('disabled');
            this.ui.liNext.addClass('disabled');
            this.ui.liPrev.addClass('disabled');
        },
        setActive: function(){
            this.ui.liFirst.removeClass('disabled');
            this.ui.liLast.removeClass('disabled');
            this.ui.liNext.removeClass('disabled');
            this.ui.liPrev.removeClass('disabled');
        }


    });

});