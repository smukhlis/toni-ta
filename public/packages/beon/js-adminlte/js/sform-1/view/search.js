/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    "text!sform1/template/search.html",
    'bootstrap'
], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template : _.template(template),
        className : 'col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2',

        ui:{
            label:'.label-serach',
            input:'.input-search'
        },

        events:{
          'click ul li a':'setBy',
          'click .act-search':'search',
          'keyup .input-search':'keyupInput'
        },

        initialize: function () {
            _.bindAll(this, 'onRender');
            this.fieldSearch = APP.formSearch.default;
        },
        onRender: function () {

        },
        setBy: function (e) {
            e.preventDefault();
            //e.currentTarget.attributes
            var el = $(e.currentTarget);
            //console.log($(e.currentTarget).attr('data-name'));
            this.ui.label.html(el.html());
            this.fieldSearch = el.attr('data-name');
        },
        search: function(e){
            if(e)e.preventDefault();
            //console.log(this.ui.input.val());
            this.trigger("search", {value: this.ui.input.val(), field:this.fieldSearch});
        },
        keyupInput: function(){
            if(event.keyCode == 13){
                this.search();
            }
        },
        setVisual: function(name, label, val){
            this.fieldSearch=name;
            this.ui.label.html(label);
            this.ui.input.val(val);
        }

    });

});