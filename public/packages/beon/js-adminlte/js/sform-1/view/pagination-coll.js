/**
 *
 */
define([
    'sform1/view/pagination'
], function(Pagination) {

    return Pagination.extend({

        initialize: function () {
            Pagination.prototype.initialize.apply(this, arguments);
            _.bindAll(this, 'setPaginator');
        },

        setPageCurrent: function(number, isOffline){
            var t = this;
            if(isOffline==true){
                Pagination.prototype.setPageCurrent.apply(t, [number]);
            }else{
                this.paginator.getPage(number).done(function(){
                    console.log(t.paginator);
                    Pagination.prototype.setPageCurrent.apply(t, [number]);
                });
            }



        },

        setPaginator: function (collection) {
            this.paginator = collection;
            collection.paginationView=this;
            console.log(collection);
            this.setTotalRecords(collection.state.totalRecords);
        }


    });

});