/**
 *
 */
define(
    [
        "jquery",
        "underscore",
        "marionette",
        "text!sform1/template/modal-confirm.html",
        "bootstrap"
    ],
    function ($, _, Marionette, template) {


        return Marionette.ItemView.extend({
            defaults: {
                title: 'Confirm delete',
                content: 'Are you sure you want to delete this item ?'
            },
            template: _.template(template),
            ui:{
              modal:'#confirm-modal'
            },
            events:{
                'click .confirm-action':'confirmOk'
            },
            initialize: function () {
                this.options = _.extend(this.defaults, this.options);
                _.bindAll(this,
                    "deleteItem",
                    "confirmOk",
                    "onRender"
                );
            },
            onRender: function () {

            },
            deleteItem: function (id, row) {
                console.log(id);
                console.log(this.ui.modal);
                this.ui.modal.modal('show');
                this.deleteId = id;
                this.deleteRow = row;
            },
            confirmOk: function () {
                var t=this;
                this.model.destroy().done(function(data){
                    console.log(data);
                    t.ui.modal.modal('hide');
                    t.collection.state.totalRecords=t.collection.state.totalRecords-1;
                    console.log('t.collection.state.totalRecords');
                    console.log(t.collection.state.totalRecords);


                    // pag
                    t.collection.paginationView.setTotalRecords(t.collection.state.totalRecords);
                    console.log(t.collection.paginationView.pageCurrent);
                    console.log(t.collection.paginationView.pageLast);
                    if(t.collection.paginationView.pageCurrent >= t.collection.paginationView.pageLast){
                        t.collection.paginationView.setPageCurrent(t.collection.paginationView.pageLast-1);
                    }else{
                        t.collection.paginationView.setPageCurrent(t.collection.paginationView.pageCurrent);
                    }

                });

//                $.ajax({
//                    url: window.location.href.toString()+"/"+t.deleteId,
//                    type: 'DELETE',
//                    //data: { id: t.deleteId },
//                    success: function(result) {
//                        t.ui.modal.modal('hide');
//                        console.log(result);
//                        console.log(oTable);
//                        var aPos = oTable.fnGetPosition(t.deleteRow);
//
//                        oTable.fnDeleteRow(aPos);
//                    }});

            },
            serializeData: function(){
                var data = {};
                data.title = this.options.title;
                data.content = this.options.content;
                return data;
            }

        });
    });