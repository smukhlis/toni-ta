/**
 *
 */
define([
    'jquery',
    'underscore',
    'sform1/view/checkbox-group'
], function ($, _, CheckboxGroup) {

    return CheckboxGroup.extend({
        defaults: {
            label: 'Label',
            type: 'radio',
            name: 'name',
            dataItem: [
                {
                    value:1,
                    label:'Satu'
                },
                {
                    value:2,
                    label:'Dua'
                },
                {
                    value:3,
                    label:'Tiga'
                }
            ]
        },
        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender');
        }

    });

});