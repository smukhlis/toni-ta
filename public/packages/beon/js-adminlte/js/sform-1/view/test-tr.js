/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    'sform1/view/edit-form-layout-1',
    'sform1/view/modal-delete'
], function($, _, Marionette, Form, Modal) {

    return Marionette.ItemView.extend({
        template: "#tr-tpl",
        tagName: "tr",

        events:{
            'click .act-delete':'actDelete',
            'click td':'openOrCloseForm'
        },

        isFormVisible:true,

        initialize: function () {
            _.bindAll(this, 'onRender', 'actDelete', 'openOrCloseForm');
            this.listenTo(this.model, 'change', this.render, this);
        },
        openOrCloseForm: function(e){
            var t = this;
            console.error('err');
            console.error(t.form);
            if(e)e.preventDefault();
            console.log(t.model);

            if(typeof t.form === "undefined" ||t.form==null || _.isEmpty(t.form)){
                t.form = new Form({title:'Edit ' + APP.nameList});
                t.form.model= t.model;
                t.form.render();
                t.form.trView = t;
                t.$el.after(t.form.el);
                _.each(APP.formField, function(e){
                    t.form.$el.find('#'+e).val(t.model.get(e));
                });
                //$(":checkbox[value='wed']").attr('checked', true);
                _.each(APP.dataField, function(e,i){
                    console.log(e);
                    console.log(i);
                    if(e.field=='checkbox-group'){
                        $(":checkbox[value='"+t.model.get(i)+"']").attr('checked', true);
                    }else if(e.field=='radio-group'){
                        $(":radio[value='"+t.model.get(i)+"']").attr('checked', true);
                    }else if(e.field=='radio' || e.field=='checkbox'){
                        if(t.model.get(i)==1){
                            t.form.$el.find('#'+i).attr('checked', true);
                        }
                    }
                    // t.form.$el.find('#'+e).val(t.model.get(e));
                });
            }else{

                t.form.destroy();
                delete t.form;
            }
            //console.log(t.form);
        },
        onRender: function () {
            var t = this;

        },
        onBeforeDestroy:function(){
            if(this.form){
                this.form.destroy();
            }
        }
        ,
        actDelete:function(e){
            var t = this;
            if(e)e.preventDefault();
            if(!APP.modalDelete){
                APP.modalDelete = new Modal();
                APP.modalDelete.render();
                $('body').append(APP.modalDelete.el);
            }
            APP.modalDelete.model = this.model;
            console.log(this.options);
            APP.modalDelete.collection = this.options.parentView.collection;
            console.log(APP.modalDelete.collection);
            APP.modalDelete.ui.modal.modal();
            return false;
        }

    });

});