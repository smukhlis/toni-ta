/**
 *
 */
define([
    'jquery',
    'underscore',
    'sform1/view/checkbox'
], function ($, _, Checkbox) {

    return Checkbox.extend({
        defaults: {
            label: 'Label',
            type: 'radio',
            name: 'name'
        },

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender');
        }

    });

});