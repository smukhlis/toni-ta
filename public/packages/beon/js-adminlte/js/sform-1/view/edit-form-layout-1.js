/**
 *
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'sform1/view/form-layout-1'
], function($, _, Backbone, Marionette, Form) {

    return Marionette.ItemView.extend({
        defaults: {
            title: ''
        },
        template : _.template('<td style="background: #fff;" colspan="'+(APP.formField.length+2)+'"></td>'),
        tagName : 'tr',

        ui:{
          cForm:'td'
        },

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender','onCancel');
        },
        onRender: function () {
            var t = this;
            this.form = new Form({title:this.options.title});
            this.form.render();
            this.form.model= t.model;
            this.form.on('cancel', t.onCancel);
            this.ui.cForm.append(this.form.el);
        },
        onCancel: function(){
            console.log('ss===================');
            this.form.destroy();
            delete this.form;
            this.trView.form.destroy();
            delete this.trView.form;
        }

    });

});