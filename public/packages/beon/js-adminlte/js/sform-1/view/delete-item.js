/**
 * use : admin1\controllers\page\group.js
 */
define(
    [
        "jquery",
        "underscore",
        "marionette",
        "text!app/template/modal-confirm.html",
        "bootstrap"
    ],
    function ($, _, Marionette, template) {


        return Marionette.ItemView.extend({
            template: _.template(template),
            ui:{
              modal:'#confirm-modal'
            },
            events:{
                'click .confirm-action':'confirmOk'
            },
            initialize: function () {
                _.bindAll(this,
                    "deleteItem",
                    "confirmOk",
                    "onRender"
                );
            },
            onRender: function () {

            },
            deleteItem: function (id, row) {
                console.log(id);
                console.log(this.ui.modal);
                this.ui.modal.modal('show');
                this.deleteId = id;
                this.deleteRow = row;
            },
            confirmOk: function () {
                console.log(window.location.href.toString()+"/"+this.deleteId);
                var t=this;

                $.ajax({
                    url: window.location.href.toString()+"/"+t.deleteId,
                    type: 'DELETE',
                    //data: { id: t.deleteId },
                    success: function(result) {
                        t.ui.modal.modal('hide');
                        console.log(result);
                        console.log(oTable);
                        var aPos = oTable.fnGetPosition(t.deleteRow);

                        oTable.fnDeleteRow(aPos);
                    }});

            }

        });
    });