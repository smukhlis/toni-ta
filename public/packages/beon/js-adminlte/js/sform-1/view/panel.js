/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    'text!sform1/template/panel.html',
], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        defaults: {
            title: '',
            classBody: 'col-lg-12'
        },
        template : _.template(template),
        className : 'box box-primary',

        ui:{
          cBtnHead : '.c-btn-head',
          cBtnFoot : '.c-btn-foot',
          cBody : '.c-body'
        },
        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender');
        },
        onRender: function () {
        },
        serializeData: function(){
            var data = {};
            data.title = this.options.title;
            data.classBody = this.options.classBody;
            return data;
        }

    });

});