/**
 *
 */
define([
    'jquery',
    'underscore',
    'sform1/view/textfield',
    "text!sform1/template/textarea.html"
], function($, _, Textfield, template) {
    return Textfield.extend({
        template : _.template(template),
        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender');
        }
    });

});