/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    'sform1/view/test-tr',
    'sform1/view/search'
], function($, _, Marionette, Tr, Search) {

    return Marionette.CompositeView.extend({
        template: "#table-tpl",
        childView: Tr,
        childViewContainer: 'tbody',

        ui:{
            cSearch:'.c-search',
            th:'th'
        },

        events:{
            'click th':'clickTh'
        },

        initialize: function () {
            this.childViewOptions={parentView:this};
            _.bindAll(this, 'onRender', 'onSearch', 'fetchSort', 'setSearch');
            var t = this;
            t.valSearch='';
            t.fieldSearch=APP.formSearch.default;
            t.labelSearch=APP.friendlyNames[APP.formSearch.default];
        },
        onRender: function () {
            this.search = new Search();
            this.search.render();

            this.search.on('search', this.onSearch);

            this.ui.cSearch.append(this.search.el);

        },
        onSearch: function(params){
            var t = this;

            t.valSearch=params.value;
            t.fieldSearch=params.field;
            t.labelSearch=APP.friendlyNames[t.fieldSearch];

            console.log('params.value');
            if(params.value){
                this.collection.queryParams[params.field]=params.value;
                this.collection.queryParams._search=true;
            }else{
                delete this.collection.queryParams._search;
            }

            this.collection.queryParams._end=true;
            this.collection.fetch().done(function(){
                console.log(t.collection);
                t.collection.paginationView.setTotalRecords(t.collection.state.totalRecords);
                t.collection.paginationView.setPageCurrent(t.collection.state.lastPage, true);
                delete t.collection.queryParams._end;

                t.search.setVisual(t.fieldSearch, t.labelSearch, t.valSearch);

                console.log(t.collection);
            });

        },
        clickTh: function (e) {
            console.error(e.currentTarget);
            var t = this;
            var item = $(e.currentTarget);
            var name = item.attr('data-name');
            var sort = item.attr('data-sort');

            var itemId = undefined;

            var newName = name;
            var newSort = 'asc';

            if(sort=='none'){
                newSort='asc';
            }else if(sort=='asc'){
                newSort='desc';
            }else if(sort=='asc'){
                newName='id';
            }

            //t.fetchSort(newName, newSort);

            t.collection.state.sortKey=newName;
            t.collection.queryParams._sort=newName;
            t.collection.queryParams._order=newSort;




            t.collection.getPage(t.collection.state.currentPage).done(function(){
                console.log(t.collection);

                t.ui.th.each(function(i,e1){
                    console.log(e1);
                    var item1 = $(e1);
                    var name1 = item1.attr('data-name');



                    if(name1=='id'){
                        itemId=item1;
                    }
                    //console.log(name);
                    //console.log(name1);
                    if(name==name1){
                        console.log('dd');
                        if(sort=='none'){
                            item1.attr('data-sort','asc');
                            item1.find('i').attr('class', 'fa fa-sort-asc');
                            //t.fetchSort(name1, 'asc');
                            //newName=name1;
                            //newSort='asc';
                        }else if(sort=='asc'){
                            item1.attr('data-sort','desc');
                            item1.find('i').attr('class', 'fa fa-sort-desc');
                            //t.fetchSort(name1, 'desc');
                            //newName=name1;
                            //newSort='desc';
                        }else if(sort=='desc'){
                            item1.attr('data-sort','none');
                            item1.find('i').attr('class', 'fa fa-sort');


                            itemId.attr('data-sort','asc');
                            itemId.find('i').attr('class', 'fa fa-sort-asc');
                            //t.fetchSort('id', 'asc');
                        }
                        console.log(item1.attr('data-sort'));
                    }else{
                        item1.attr('data-sort','none');
                        item1.find('i').attr('class', 'fa fa-sort');
                    }

                    //console.log(e1);
                    //console.log(i);
                });

                t.ui.th.each(function(i,e1){
                    console.log(e1);
                });

                t.search.setVisual(t.fieldSearch, t.labelSearch, t.valSearch);

            });



            //this.setDefault();
        },
        fetchSort: function(name,order){
            var t = this;
            t.collection.queryParams._sort=name;
            t.collection.queryParams._order=order;




            t.collection.getPage(t.collection.state.currentPage).done(function(){
                console.log(t.collection);
                t.ui.th.each(function(i,e1){
                    console.log(e1);
                });

            });
        },
        setSearch: function(){
            var t = this;
            console.log(t.collection.queryParams);
        }


    });

});