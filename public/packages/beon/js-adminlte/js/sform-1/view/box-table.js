/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    'text!sform1/template/box-table.html',
    'sform1/view/form-layout-1'
], function($, _, Marionette, template, Form) {

    return Marionette.ItemView.extend({
        defaults: {
            title: 'Title'
        },
        template : _.template(template),
        className : 'box',

        ui:{
          body:'.box-body'
        },

        events:{
          'click .act-add':'actAdd'
        },

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender', 'actAdd', 'appendTable');
        },
        onRender: function () {

        },
        actAdd: function () {
            if(!this.form){
                this.form = new Form({title:'Add '+APP.nameList});
                this.form.render();
                this.form.collection=this.table.collection;
                this.ui.body.prepend(this.form.el);
            }else{
                this.form.$el.show();
            }

        },
        serializeData: function(){
            var data = {};
            data.title = this.options.title;
            return data;
        }
        ,
        appendTable: function(tbl){
            this.ui.body.append(tbl.el);
            this.table = tbl;
        }

    });

});