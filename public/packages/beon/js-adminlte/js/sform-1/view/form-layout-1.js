/**
 *
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'text!sform1/template/form-layout-1.html',
    'sform1/model/model',
    'sform1/view/textfield',
    'sform1/view/textarea',
    'sform1/view/combobox',
    'sform1/view/checkbox',
    'sform1/view/radio',
    'sform1/view/checkbox-group',
    'sform1/view/radio-group'
], function($, _, Backbone, Marionette, template, Model, Textfield, Textarea, Combobox, Checkbox, Radio, CheckboxGroup, RadioGroup) {

    return Marionette.ItemView.extend({
        defaults: {
            title: ''
        },
        template : _.template(template),
        tagName : 'form',
        className:'col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3',

        ui:{
          body:'.box-body'
        },

        events:{
          'click .act-save':'actSave',
          'click .act-cancel':'actCancel',
          'click .act-reset':'actReset'
        },

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender', 'addTextfield', 'actSave', 'addTextarea', 'actCancel', 'actReset',
                'addCombobox', 'addCheckbox', 'addRadio', 'addRadioGroup');
        },
        onRender: function () {
            var t = this;
            _.each(APP.formField, function(e){
                if(!APP.dataField[e]){
                    t.addTextfield(e, APP.friendlyNames[e]);
                }else if(APP.dataField[e]['field']=='textfield'){
                    t.addTextfield(e, APP.friendlyNames[e]);
                }else if(APP.dataField[e]['field']=='textarea'){
                    t.addTextarea(e, APP.friendlyNames[e]);
                }else if(APP.dataField[e]['field']=='combobox'){
                    t.addCombobox(e, APP.friendlyNames[e], APP.dataField[e]['dataList']);
                }else if(APP.dataField[e]['field']=='checkbox'){
                    t.addCheckbox(e, APP.friendlyNames[e]);
                }else if(APP.dataField[e]['field']=='radio'){
                    t.addRadio(e, APP.friendlyNames[e]);
                }else if(APP.dataField[e]['field']=='checkbox-group'){
                    t.addCheckboxGroup(e, APP.friendlyNames[e], APP.dataField[e]['dataItem']);
                }else if(APP.dataField[e]['field']=='radio-group'){
                    t.addRadioGroup(e, APP.friendlyNames[e], APP.dataField[e]['dataItem']);
                }
            });
        },
        actCancel: function (e) {
            if(e)e.preventDefault();
            this.$el.hide();
            this.actReset();
            this.trigger("cancel");
            return false;
        },
        actReset: function (e) {
            if(e)e.preventDefault();
            var t = this;
            _.each(APP.formField, function(e1){
                t.$el.find('#'+e1).val('');
            });
            return false;
        },
        addTextfield: function (name, label) {
            var tf = new Textfield({name:name, label:label});
            tf.render();
            this.ui.body.append(tf.el);
        },
        addCheckbox: function (name, label) {
            var tf = new Checkbox({name:name, label:label});
            tf.render();
            this.ui.body.append(tf.el);
        },
        addCheckboxGroup: function (name, label, dataItem) {
            var tf = new CheckboxGroup({name:name, label:label, dataItem:dataItem});
            tf.render();
            this.ui.body.append(tf.el);
        },
        addRadio: function (name, label) {
            var tf = new Radio({name:name, label:label});
            tf.render();
            this.ui.body.append(tf.el);
        },
        addRadioGroup: function (name, label, dataItem) {
            var tf = new RadioGroup({name:name, label:label, dataItem:dataItem});
            tf.render();
            this.ui.body.append(tf.el);
        },
        addTextarea: function (name, label) {
            var tf = new Textarea({name:name, label:label});
            tf.render();
            this.ui.body.append(tf.el);
        },
        addCombobox: function (name, label, dataList) {
            var tf = new Combobox({name:name, label:label, dataList:dataList});
            tf.render();
            this.ui.body.append(tf.el);
        },
        actSave: function (e) {
            var t = this;
            this.$el.find('.help-error').remove();
            this.$el.find('.has-error').removeClass('has-error');
            if(e)e.preventDefault();
            if(!t.model){
                t.model = new Model();
            }

            var arr = this.$el.serializeArray();
            console.log(arr);
            var data = _(arr).reduce(function(acc, field) {
                if(acc[field.name]){
                    acc[field.name]=acc[field.name]+','+field.value
                }else{
                    acc[field.name] = field.value;
                }
                return acc;
            }, {});
            var isNew = true;
            if(!t.model.isNew())isNew = false;
            t.model.save(data).done(function(data){
                if(isNew){
                    if(t.collection){
                        var p = t.collection.paginationView;
                        p.goToPage(p.pageLast-p.pageFirst);

                    }
                    delete t.model;
                }
                t.actCancel();

            }).fail(function(jqXHR){
                console.log(data);
                console.log(jqXHR);
                for(var key in jqXHR.responseJSON){
                    console.log(key);
                    var field = $('#'+key);
                    field.after('<span class="help-block help-error">'+jqXHR.responseJSON[key][0]+'</span>');
                    field.parent().addClass('has-error');
                }
            });
            return false;
        },
        serializeData: function(){
            var data = {};
            data.title = this.options.title;
            return data;
        }
    });

});