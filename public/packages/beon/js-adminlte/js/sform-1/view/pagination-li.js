/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
], function($, _, Marionette) {

    return Marionette.ItemView.extend({
        defaults: {
            pageNumber: 1,
            pagination: false
        },
        template : _.template('<a href="#"><%=pageNumber%></a>'),
        tagName : 'li',

        ui:{
          a:'a'
        },

        events:{
          'click a':'goToPage'
        },

        goToPage: function(e){
            e.preventDefault();
            if(this.options.pagination!=false)this.options.pagination.goToPage(parseInt(this.ui.a.html()));
            return false;
        },

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender', 'goToPage', 'setPageNumber');
        },
        onRender: function () {

        },
        serializeData: function(){
            var data = {};
            data.pageNumber = this.options.pageNumber;
            return data;
        },
        setPageNumber: function (number) {
            this.ui.a.html(number);
        }

    });

});