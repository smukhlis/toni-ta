/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette'
], function ($, _, Marionette) {

    return Marionette.ItemView.extend({
        template : _.template('<label class="control-label" for="<%= name %>"><%= label %></label>'),
        className : 'form-group',
        defaults: {
            label: 'Label',
            type: 'checkbox',
            name: 'name',
            dataItem: [
                {
                    value:1,
                    label:'Satu'
                },
                {
                    value:2,
                    label:'Dua'
                },
                {
                    value:3,
                    label:'Tiga'
                }
            ]
        },

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender');
        },
        onRender: function () {
            var t = this;
            _.each(t.options.dataItem, function(e, i){
                t.$el.append('<div class="checkbox"><p><input value="'+ e.value +'" id="'+ t.options.name+i+'" name="'+ t.options.name+'" type="'+ t.options.type+'"><label class="lbl" for="'+ t.options.name+i+'">'+ e.label+'</label></p></div>');
            });
        },
        serializeData: function(){
            var data = {};
            data.label = this.options.label;
            data.name = this.options.name;
            data.type = this.options.type;
            return data;
        }

    });

});