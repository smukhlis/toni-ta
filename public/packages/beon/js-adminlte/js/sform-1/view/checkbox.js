/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    'text!sform1/template/checkbox.html'
], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        defaults: {
            label: 'Label',
            type: 'checkbox',
            name: 'name'
        },
        template : _.template(template),
        className : 'form-group',

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender');
        },
        onRender: function () {

        },
        serializeData: function(){
            var data = {};
            data.label = this.options.label;
            data.name = this.options.name;
            data.type = this.options.type;
            return data;
        }

    });

});