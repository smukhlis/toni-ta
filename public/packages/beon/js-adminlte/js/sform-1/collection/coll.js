define(
    [
        'backbone',
        'paginator',
        'sform1/model/model'
    ],
    function (Backbone, Paginator, Model) {
        return Paginator.extend({
            model: Model,

            // Enable infinite paging
            //mode: "infinite",

            url: APP.restUrl,

            // Initial pagination states
            state: {
                pageSize: 20,
                sortKey: "id",
                order: 'asc'
            },

            // You can remap the query parameters from `state` keys from
            // the default to those your server supports
            queryParams: {
                totalPages: '_total_pages',
                currentPage: '_page',
                pageSize: '_page_size',
                order: '_order',
                totalRecords: '_total_records',
                sortKey: "_sort"
            },
            parseState: function (resp, queryParams, state, options) {
                console.log(options.xhr.getResponseHeader("X-page"));
                return {
                    totalRecords: parseInt(options.xhr.getResponseHeader("X-total")),
                    totalPages: parseInt(options.xhr.getResponseHeader("X-total-page")),
                    currentPage: parseInt(options.xhr.getResponseHeader("X-page"))
                };
            }
        });
    }
);
