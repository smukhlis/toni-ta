define(
    [
        'backbone',
        'paginator',
        'sform1/model/user'
    ],
    function (Backbone, Paginator, User) {
        return Paginator.extend({
            model: User,

            // Enable infinite paging
            //mode: "infinite",

            url: BASE_URL + '/dashboard/editor-jh/logos',

            // Initial pagination states
            state: {
                pageSize: 2,
                sortKey: "id",
                order: 1
            },

            // You can remap the query parameters from `state` keys from
            // the default to those your server supports
            queryParams: {
                totalPages: null,
                totalRecords: null,
                sortKey: "sort"
            },
            parseState: function (resp, queryParams, state, options) {
                console.log(options.xhr.getResponseHeader("X-total"));
                return {totalRecords: parseInt(options.xhr.getResponseHeader("X-total"))};
            }
        });
    }
);
