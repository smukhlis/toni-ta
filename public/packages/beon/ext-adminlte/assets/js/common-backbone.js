//The build will inline common dependencies into this file.

//For any third party dependencies, like jQuery, place them in the lib folder.

//Configure loading modules from the lib directory,
//except for 'app' ones, which are in a sibling
//directory.
requirejs.config({
    baseUrl: BASE_URL + '/assets/js/lib',
    paths: {
        app: '../app',
        //packages: '../../../packages',
        bootstrap: '../../../packages/jakubsacha/adminlte/AdminLTE/js/bootstrap.min',
        //jquery: '../lib/jquery/jquery-2.1.0.min',
        jquery: 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min',
        jqueryUI: '../lib/jquery-ui/jquery-ui-1.10.4.custom.min',
        migrate: 'jquery/jquery-migrate-1.2.0.min',
        underscore: '../lib/underscore/underscore',
        underscoreString: '../lib/underscore/underscore.string',
        backbone: '../lib/backbone/backbone',
        marionette: '../lib/backbone/backbone.marionette',
        text: '../lib/require/text',
        domReady: '../lib/require/domReady',
        datepicker: '../lib/datepicker/bootstrap-datepicker',
        autocomplete: '../lib/autocomplete/jquery.autocomplete.min',
        chart: '../lib/chart/nv.d3.min',
        elfinder: '../lib/elfinder/elfinder.min',
        daterangepicker: '../lib/daterangepicker/daterangepicker',
        json: '../lib/utils/json2'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        marionette: {
            exports: 'Backbone.Marionette',
            deps: ['backbone']
        },
        jqueryUI: {
            deps: ['jquery', 'migrate']
        },
        bootstrap: {
            deps: ['jquery', 'migrate']
        }
    }
});
