(function($){
    $.fn.extend({

        //pass the options variable to the function
        browseImage: function(options) {
            //Set the default values, use comma to separate the settings, example:
            var defaults = {
                url: "",
                classParentImage: ".thumbnail",
                idImageSelector : "#image-selected",
                idBrowseFile : "#file-input"
            }

            options = $.extend(defaults, options);
            return this.each(function() {
                var t = this;
                var o = options;
                var dialog;
                $(t).click(function(){
                    if (!dialog) {
                        var dialog = $("<div></div>").dialogelfinder({
                            url : o.url,
                            commandsOptions: {
                                getfile: {
                                    oncomplete : "close" // close/hide elFinder
                                }
                            },
                            width: $(window).width()-20,
                            height: $(window).height()-20,
                            getFileCallback: function(file){
                                $(t).parent().prev(o.classParentImage).children(o.idImageSelector).attr("src", file.url);
                                $(t).prev(o.idBrowseFile).val(file.url);
                            }
                        });
                    }else {
                        // reopen elFinder
                        dialog.dialogelfinder("open");
                    }
                });

            });
        }
    });

})(jQuery);