define([
    'jquery',
    'underscore',
    'marionette',
    'text!app/view/banner/test.html',
    "codemirror",
    "showHint",
    "htmlmixed",
    "xmlHint",
    "htmlHint"
], function($, _, Marionette, template, CodeMirror) {

    return Marionette.ItemView.extend({
        template : _.template(template),

        ui: {
            textarea: 'textarea'
        },

        initialize: function () {
            _.bindAll(this, 'onRender', 'changeBackground');
        },

        onRender: function () {

            var t = this;
            this.cm = CodeMirror.fromTextArea(this.ui.textarea[0], {
                lineNumbers: true,
                mode: "htmlmixed",
                extraKeys: {"Ctrl-Space": "autocomplete"},
                viewportMargin: 20,
                theme: 'monokai'
            });

            t.frame = $("#iframe1").contents();
            t.content = t.frame.find('.header-banner .slider.item .container');
            this.cm.on("change", function(){
                t.content.html(t.cm.getValue());
                /*iframe.on("load", function () {
                    console.log(t.cm.getValue());

                });*/
            });

        },
        changeBackground: function (img) {
            this.content.css('background', 'url("'+img+'") no-repeat 23px center');
        }
    });

});