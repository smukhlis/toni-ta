
define([
    'jquery', 'datepicker', 'autocomplete'
], function ($) {
    $(document).on('submit', '#update-form', function()
    {
        $('.help-error').remove();
        $('.has-error').removeClass('has-error');
        var sArray = $(this).serializeArray();
        console.log(sArray);
        $.ajax({
            "type": "PUT",
            "url": window.location.href.toString().replace('/edit',''),
            "data": sArray,
            "dataType": "json"
        }).done(function(result)
        {
            console.log(result);
            if(result.status === false)
            {
                if(typeof result.message !== 'undefined')
                {
                    showStatusMessage(result.message, result.messageType);
                }
                else if(typeof result.errorMessages !== 'undefined')
                {
                    showRegisterFormAjaxErrors(result.errorMessages);
                }
            }
            else
            {
                window.location = result.redirectUrl;
            }
        }).fail(function (jqXHR) {
            console.log(jqXHR.responseJSON);
            for(var key in jqXHR.responseJSON){
                console.log(key);
                var field = $('#'+key);
                field.after('<span class="help-block help-error">'+jqXHR.responseJSON[key][0]+'</span>');
                field.parent().parent().addClass('has-error');
            }
        });

        return false;
    });

    //$('#paket').autocomplete({	lookup: PAKETS, 	minChars:0});
});
