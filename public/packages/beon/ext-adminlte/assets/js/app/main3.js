define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {

    return Backbone.Model.extend({
        urlRoot: BASE_URL + '/user1/users',

        defaults: {
            status: 1
        },

        initialize: function () {

        }

    });

});
