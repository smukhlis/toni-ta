$(function() 
{
    $('.activate-customer').tooltip();

    $(document).on('submit', '#create-customer-form', function()
    {
        $('.help-error').remove();
        $('.has-error').removeClass('has-error');
        var sArray = $(this).serializeArray();
        console.log(sArray);
        $.ajax({
            "type": "POST",
            "url": window.location.href.toString().replace('/create',''),
            "data": sArray,
            "dataType": "json"
        }).done(function(result)
        {
            console.log(result);
            if(result.customerCreated === false)
            {
                if(typeof result.message !== 'undefined')
                {
                    showStatusMessage(result.message, result.messageType);
                }
                else if(typeof result.errorMessages !== 'undefined')
                {
                    showRegisterFormAjaxErrors(result.errorMessages);
                }
            }
            else
            {
                window.location = result.redirectUrl;
            }
        }).fail(function (jqXHR) {
            console.log(jqXHR.responseJSON);
            for(var key in jqXHR.responseJSON){
                console.log(key);
                var field = $('#'+key);
                field.after('<span class="help-block help-error">'+jqXHR.responseJSON[key][0]+'</span>');
                field.parent().parent().addClass('has-error');
            }
        });
        
        return false;
    }).on('submit', '#edit-customer-form', function()
    {
        var sArray = $(this).serializeArray();
        $.ajax({
            "type": "PUT",
            "url": window.location.href.toString(),
            "data": sArray,
            "dataType": "json"
        }).done(function(result)
        {
            if(typeof result.message !== 'undefined')
            {
                showStatusMessage(result.message, result.messageType);

                if(result.messageType == 'success')
                {
                    ajaxContent($(this).attr('href'), ".ajax-content", false);
                }
            }
            else if(typeof result.errorMessages !== 'undefined')
            {
                showRegisterFormAjaxErrors(result.errorMessages);
            }
        });

        return false;
    }).on('click', '#delete-item', function()
    {
        $('#confirm-modal').modal();
    }).on('click', '.delete-customer .confirm-action', function()
    {
        $.each($('.table tbody tr td input:checkbox:checked'), function( key, value ) 
        {
            $.ajax(
            {
                "url": window.location.href.toString()+"/../customer/"+$(this).data('customer-id'),
                "type": "DELETE"
            }).done(function(result)
            {
                showStatusMessage(result.message, result.messageType);
                ajaxContent($(this).attr('href'), ".ajax-content", false);
            });
        });

        $('#confirm-modal').modal('hide');
    }).on('click', '.activate-customer', function()
    {
        var customerId = $(this).parent().parent().find('input[type="checkbox"]').data('customer-id');

        $.ajax({
            "type": "PUT",
            "url": window.location.href.toString()+'/../customer/'+customerId+'/activate/',
            "data": {customerId : customerId},
            "dataType": "json"
        }).done(function(result)
        {
            if(typeof result.message !== 'undefined')
            {
                showStatusMessage(result.message, result.messageType);

                if(result.messageType == 'success')
                {
                    ajaxContent($(this).attr('href'), ".ajax-content", false);
                }
            }
        });

        return false;
    });
});