<!DOCTYPE html>
<html class="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <title>Hosting Indonesia &amp; Domain Murah - JagoanHosting</title>
    <base href="http://www.jagoanhosting.com">
    <link rel="author" href="http://plus.google.com/+Jagoanhostingindonesia">
    <link rel="publisher" href="http://plus.google.com/+Jagoanhostingindonesia">
    <link rel="icon" href="http://www.jagoanhosting.com/images/logo_16.ico" type="image/x-icon">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Roboto:400,700" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" type="text/css" href="/assets/css/all_css.css">
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
</head>
<body id="top-class" data-twttr-rendered="true">

<section id="cloud-header-box">
    <div class="item-bg">
        <div class="item-cloud item-one animate"></div>
        <div class="item-cloud item-two animate"></div>
        <div class="item-cloud item-tri animate"></div>
        <div class="item-cloud item-for animate"></div>
        <div class="item-cloud item-fiv animate"></div>
    </div>
</section>
<div id="float_box" class="toTop" onclick="$('html, body').animate({ scrollTop: 0 }, 600);">
    <div id="float_bot" style="">
        <div class="detail">
            <a class="close">x</a>
            <a class="toTop">Back to Top</a>
        </div>
    </div>
</div>
<section id="header" class="index">


<div class="header-banner">
<div class="header-banner-slider">

<div class="flex-viewport" style="overflow: hidden; position: relative;">
<ul class="slides"
    style="width: 1600%; -webkit-transition: 0s; transition: 0s; -webkit-transform: translate3d(-6745px, 0px, 0px);">
<li class="clone" aria-hidden="true" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-agustusan">
        <div class="container">
            <div class="box-txt">
                <p class="ban-top-title" style="color:#e67e22">Program Parade Agustusan</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Parade Agustusan Bersama BEON</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Ikuti Kuisnya Menangkan Hadiahnya</p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title" style="color:#5f3d14;font-size:16px">Mari ramaikan momen kemerdekaan Indonesia
                    ke 69th bersama Beon Intermedia,<br>dengan mengikuti kuis-kuis seputar kemerdekaan di sosial media
                    BEON</p>
                <a href="https://www.facebook.com/notes/jagoan-hosting-indonesia/tata-cara-dan-peraturan-lomba-kuis-agustusan/790024731019866"
                   class="btn lig-green" target="_blank">Selengkapnya</a>
            </div>
        </div>
    </div>
</li>
<!--
<li>
  <div class="slider item item-for">
    <div class="container">
    <div class="box-txt">
      <p class="ban-top-title">Jagoan Hosting Indonesia Mengucapkan</p>
      <p class="ban-big-title">Selamat Memperingati Hari</p>
      <p class="ban-big-title">Isra' Miraj Nabi Muhammad SAW</p>
      <p class="ban-sub-title">Selamat Menempuh Perjalanan untuk Menjadi Pribadi yang</p>
      <p class="ban-sub-title">Semakin Beriman, Bertaqwa dan Berakhlaqul Karimah</p>
    </div>
    </div>
  </div>
</li>
-->


<li class="" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-anything-id">
        <div class="container">
            <div class="box-txt" style="width:600px">
                <p class="ban-top-title" style="color:#e67e22">Tebaru di jagoan Hosting Indonesia</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Domain ekstensi anything.id</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Hanya 500rb/tahun</p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title" style="color:#5f3d14;font-size:16px">Dapatkan sekarang! Orang Indonesia bangga
                    pakai domain .ID </p>
                <a href="http://www.jagoanhosting.com/domain-id" class="btn lig-blue" target="_blank">Beli Domain
                    .ID</a>
            </div>
        </div>
    </div>
</li>

<li class="" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-two">
        <div class="container">
            <div class="box-txt">
                <p class="ban-top-title">TERBARU DI JAGOAN HOSTING INDONESIA</p>

                <p class="ban-big-title">Harga Sama, Fitur lebih WOW!</p>

                <p class="ban-sub-title">di Paket Reseller &amp; Mail Hosting</p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title"><b>Bayarnya sama, dapatnya lebih luar biasa</b></p>
                <a href="reseller-hosting-indonesia" class="btn lig-blue">Reseller Hosting ID</a>
                <a href="reseller-hosting-murah" class="btn lig-blue-two">Reseller Hosting US</a>
                <a href="email-hosting-indonesia" class="btn lig-green">Email Hosting</a>
            </div>
        </div>
    </div>
</li>


<!--
<li>
  <div class="slider item item-piala">
    <div class="container">
      <div class="box-txt" style="left:350px">
          <p class="ban-top-title">Promo Piala Dunia 2014</p>
          <p class="ban-big-title" style="font-size:40px">Hosting Gratis untuk Pendukung<br/>
Tim Piala Dunia Jerman</p>
          <p class="ban-sub-title" style="font-size:17px">Apakah kamu memilih tim Jerman di Promo Piala Dunia? Cek sekarang apakah<br/>
kamu menjadi salah satu orang yang beruntung mendapatkan Hosting Gratis</p>
          <br class="clear" />
          <a href="http://beon.co.id/piala-dunia" class="btn lig-green" target="_blank">Lihat Pemenang</a>
      </div>
    </div>
  </div>
</li>
-->


<!--
<li>
  <div class="slider item item-id-memilih">
    <div class="container">
      <div class="box-txt">
          <p class="ban-top-title">Promo Indonesia memilih</p>
          <p class="ban-big-title" style="font-size:40px; margin-bottom: 2px;">Tentukan Pilihanmu Sekarang
untuk Indonesia yang Lebih Baik</p>
        <p class="ban-green-line">&nbsp;</p>
          <p class="ban-mini-title"><b>Diskon 25% untuk pembelian baru dan upgrade layanan,<br/>
dari JagoanHosting dan BEON.co.id
</b></p>
          <a href="https://www.facebook.com/jagoanhosting/app_190322544333196" class="btn lig-green" target="_blank">Tentukan Pilihan</a>
      </div>
    </div>
  </div>
</li>
-->


<li class="" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-one">
        <div class="container">
            <div class="box-txt">
                <p class="ban-top-title">Terbaru di Jagoan Hosting Indonesia</p>

                <p class="ban-big-title">Web Hosting Murah</p>

                <p class="ban-sub-title">harga mulai <b>5rb/bulan</b></p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title">Tersedia unlimited bandwidth untuk <b>Hosting ID &amp; Hosting US</b></p>
                <a href="hosting-indonesia" class="btn orange">BELI HOSTING ID</a>
                <a href="hosting-murah" class="btn yellow">BELI HOSTING US</a>
            </div>
        </div>
    </div>
</li>

<li class="" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-weekend-ceria">
        <div class="container">
            <div class="box-txt">
                <p class="ban-top-title" style="color:#e67e22">Promo Weekend Ceria</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">GRATIS Domain atau Cashback</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Setiap Pembelian Hosting</p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title" style="color:#5f3d14;font-size:16px">GRATIS domain (.com .org .us. my.id
                    .web.id) &amp; cashback 150.000, setiap<br>pembelian Hosting ID Silver ke atas selama 3 tahun di
                    JagoanHosting </p>
                <a href="http://www.jagoanhosting.com/promo-domain-hosting#4" class="btn lig-green" target="_blank">Selengkapnya</a>
            </div>
        </div>
    </div>
</li>

<li class="flex-active-slide" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-contest-seo">
        <div class="container">
            <div class="box-txt">
                <p class="ban-top-title" style="color:#e67e22">Article SEO Competition</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Kompetisi Artikel SEO Berhadiah</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Jutaan Rupiah dari Beon Intermedia</p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title" style="color:#5f3d14;font-size:16px">Menangkan hadiah laptop, kamera digital
                    dan smartphone dengan menulis<br>artikel SEO bertema "Hosting Terbaik untuk Bisnis Online
                    Profesional" </p>
                <a href="http://beon.co.id/kompetisi-seo-beon/" class="btn lig-green" target="_blank">Selengkapnya</a>
            </div>
        </div>
    </div>
</li>

<li class="" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-agustusan">
        <div class="container">
            <div class="box-txt">
                <p class="ban-top-title" style="color:#e67e22">Program Parade Agustusan</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Parade Agustusan Bersama BEON</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Ikuti Kuisnya Menangkan Hadiahnya</p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title" style="color:#5f3d14;font-size:16px">Mari ramaikan momen kemerdekaan Indonesia
                    ke 69th bersama Beon Intermedia,<br>dengan mengikuti kuis-kuis seputar kemerdekaan di sosial media
                    BEON</p>
                <a href="https://www.facebook.com/notes/jagoan-hosting-indonesia/tata-cara-dan-peraturan-lomba-kuis-agustusan/790024731019866"
                   class="btn lig-green" target="_blank">Selengkapnya</a>
            </div>
        </div>
    </div>
</li>

<!--  <li>
   <div class="slider item item-puasa">
     <div class="container">
     <div class="box-txt">
       <p class="ban-top-title">Segenap keluarga besar JagoanHosting mengucapkan</p>
       <p class="ban-big-title">Selamat Menunaikan Ibadah</p>
       <p class="ban-big-title">Puasa Ramadhan 1435 H</p>
       <p class="ban-sub-title">Mari sucikan hati di bulan suci tuk meraih berkah ilahi</p>
     </div>
     </div>
   </div>
 </li> -->

<!--
<li>
  <div class="slider item item-tri">
    <div class="container">
    <div class="box-txt">
      <p class="ban-top-title">Jagoan Hosting Indonesia Mengucapkan</p>
      <p class="ban-big-title">Selamat Hari Raya Waisak</p>
      <p class="ban-big-title">Tahun 2558 BE/2014</p>
      <p class="ban-sub-title">Semoga berkah kemuliaan selalu menyertai Anda</p>
    </div>
    </div>
  </div>
</li>
-->
<li class="clone" aria-hidden="true" style="width: 1349px; float: left; display: block;">
    <div class="slider item item-anything-id">
        <div class="container">
            <div class="box-txt" style="width:600px">
                <p class="ban-top-title" style="color:#e67e22">Tebaru di jagoan Hosting Indonesia</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Domain ekstensi anything.id</p>

                <p class="ban-big-title" style="color:#1d4269;font-size:36px">Hanya 500rb/tahun</p>

                <p class="ban-green-line">&nbsp;</p>

                <p class="ban-mini-title" style="color:#5f3d14;font-size:16px">Dapatkan sekarang! Orang Indonesia bangga
                    pakai domain .ID </p>
                <a href="http://www.jagoanhosting.com/domain-id" class="btn lig-blue" target="_blank">Beli Domain
                    .ID</a>
            </div>
        </div>
    </div>
</li>
</ul>
</div>
<ol class="flex-control-nav flex-control-paging">
    <li><a class="">1</a></li>
    <li><a class="">2</a></li>
    <li><a class="">3</a></li>
    <li><a class="">4</a></li>
    <li><a class="flex-active">5</a></li>
    <li><a class="">6</a></li>
</ol>
<ul class="flex-direction-nav">
    <li><a class="flex-prev" href="#">Previous</a></li>
    <li><a class="flex-next" href="#">Next</a></li>
</ul>
</div>
</div>
</section>


<!--
<div class="cak-beon-box float-cak-beon">
  <div class="box-img">
    <p class="mini-title">Tips <br/>Cak Beon</p>
    <img src="/assets/images/char-cak-beon.png" />
  </div>
  <div class="box-txt">
    <div class="desc">
      <p class="title">TIPS CAK <span>BEON</span></p>
      Tahukah Anda, jika pengunjung website Anda banyak dari Indonesia, maka akan lebih cepat jika Anda menggunakan hosting Indonesia <br/>
      <a href="hosting-indonesia">Paket Hosting Indonesia</a>
    </div>
  </div>
</div>
-->


<!-- ========================= JAVASCRIPT ============================== -->

<script type="text/javascript" src="/assets/js/js_load.js"></script>



<script src="assets/js/hoverIntents.js"></script>



<script src="assets/js/js_all.js"></script>

</body>
</html>