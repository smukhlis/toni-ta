<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\ToolsIncome\models;


use Beon\Crm\helpers\StringHelper;

class AppLog extends \Eloquent{

    protected $table = StringHelper::TABLE_APP_LOG;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'key',
        'grade'
    ];


}