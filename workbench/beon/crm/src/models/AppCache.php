<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\ToolsIncome\models;


use Beon\Crm\helpers\StringHelper;

class AppCache extends \Eloquent{

    protected $table = StringHelper::TABLE_APP_CACHE;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'key',
        'grade'
    ];


}