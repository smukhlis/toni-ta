<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Crm\models;


use Beon\Crm\helpers\StringHelper;

class SectionMutation extends \Eloquent{

    protected $table = StringHelper::TABLE_SECTION_MUTATION;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [

    ];




}