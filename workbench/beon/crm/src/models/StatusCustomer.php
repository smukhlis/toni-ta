<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Crm\models;


use Beon\Crm\helpers\StringHelper;
use Beon\JsAdminlte\models\ModelJs;

class StatusCustomer extends ModelJs{

    protected $table = StringHelper::TABLE_STATUS_CUSTOMER;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public static $formFields = [
        'name'
    ];

    public static $tblItems = [
        'name'
    ];

    public static  $friendlyNames =[
        'id'=>'Id',
        'name'=>'Name',
        'created_at'=>'Created at',
        'updated_at'=>'Updated at'
    ];

    public static $rules = [
        'name'=>'required|max:255',
    ];


    public static function searchConfig()
    {
        return [
            'default'=>'name',
            'data'=>['id', 'name']
        ];
    }

    public static function formConfig()
    {
        return [];
    }




}