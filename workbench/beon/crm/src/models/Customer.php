<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Crm\models;


use Beon\Crm\helpers\StringHelper;
use Beon\JsAdminlte\models\ModelJs;

class Customer extends ModelJs{

    protected $table = StringHelper::TABLE_CUSTOMER;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'email',
        'phone',
        'source',
        'prefix',
        'name',
        'domain',
        'date_of_birth',
        'place_of_birth',
        'state',
        'city',
        'address',
        'type_service_id'
    ];

    public static $formFields = [
        'prefix',
        'email',
        'phone',
        'source',
        'name',
        'domain',
        'date_of_birth',
        'place_of_birth',
        'state',
        'city',
        'address'
    ];

    public static $tblItems = [
        'email',
        'phone',
        'source',
        'name',
        'domain',
        'city',
    ];

    public static  $friendlyNames =[
        'id'=>'Id',
        'email'=>'Email',
        'phone'=>'Phone',
        'source'=>'Source',
        'prefix'=>'Prefix',
        'name'=>'Name',
        'domain'=>'Domain',
        'date_of_birth'=>'Date of birth',
        'place_of_birth'=>'Place of birth',
        'state'=>'State',
        'city'=>'City',
        'address'=>'Address',
        'type_service_id'=>'Type service id',
        'created_at'=>'Created at',
        'updated_at'=>'Updated at'
    ];

    public static $rules = [
        'name'=>'required|max:255',
        'email'=>'unique|required|max:255|email',
        'phone'=>'max:255',
        'source'=>'max:255',
        'prefix'=>'max:255',
        'domain'=>'max:255',
        'date_of_birth'=>'date_format:Y-m-d h:i:s',
        'place_of_birth'=>'max:255',
        'state'=>'max:255',
        'city'=>'max:255',
        'address'=>'max:255',
        'type_service_id'=>'numeric'
    ];


    public static function searchConfig()
    {
        return [
            'default'=>'name',
            'data'=>self::$formFields
        ];
    }

    public static function formConfig()
    {
        return [
            'address'=>[
                'field'=>'textarea'
            ],
            'prefix'=>[
                'field'=>'combobox',
                'dataList'=>[
                    1=>'Mr.',
                    0=>'Miss.'
                ]
            ]
        ];
    }
}