<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use Beon\Crm\helpers\RouteHelper;
use Beon\Crm\helpers\StringHelper;

Route::group([
    'before' => 'basicAuth',
    'prefix' => Config::get('syntara::config.uri').'/' . StringHelper::PACKAGE_NAME,
    'namespace'=> 'Beon\Crm\controllers'
], function()
{


    Route::get('test', 'TestController@index');
    Route::resource('customers', 'CustomersController');
    Route::get('customer/manage', 'CustomersController@manage');

    Route::resource('type-packets', 'TypePacketController');
    //Route::get('type-packet/manage', 'TypePacketController@manage');
    Route::get('type-packet/manage', [
        'uses'=>'TypePacketController@manage',
        'as'=>RouteHelper::baseName().'type-packets.manage'
    ]);

    Route::resource('type-services', 'TypeServiceController');
   // Route::get('type-service/manage', 'TypeServiceController@manage');
    Route::get('type-service/manage', [
        'uses'=>'TypeServiceController@manage',
        'as'=>RouteHelper::baseName().'type-services.manage'
    ]);

    Route::resource('status-customers', 'StatusCustomerController');

    Route::get('status-customer/manage', [
        'uses'=>'StatusCustomerController@manage',
        'as'=>RouteHelper::baseName().'.customers.manage'
    ]);


    //Route::resource('customers', 'CustomersController');
    //Route::get('customer', 'IncomeController@customer');
    //Route::get('customer', 'IncomeController@customers');
    //Route::get('mail', 'IncomeController@mail');

});
