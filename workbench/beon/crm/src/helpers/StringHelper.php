<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:18 PM
 */

namespace Beon\Crm\helpers;

use Config;
use Route;

class StringHelper
{
    const VENDOR_NAME = 'beon';
    const PACKAGE_NAME = 'crm';

    const TABLE_APP_CACHE = 'crm_app_caches';
    const TABLE_APP_LOG = 'crm_app_logs';
    const TABLE_ATTACHMENT = 'crm_attachments';
    const TABLE_CITY = 'crm_cities';
    const TABLE_CUSTOMER = 'crm_customers';
    const TABLE_CUDTOMER_LEAD = 'crm_customer_leads';
    const TABLE_FOLLOWUP = 'crm_followups';
    const TABLE_INCOME = 'crm_incomes';
    const TABLE_PAYMENT_SUGESTION = 'crm_payment_sugestions';
    const TABLE_SECTION = 'crm_sections';
    const TABLE_SECTION_MUTATION = 'crm_section_mutations';
    const TABLE_SOURCE = 'crm_sources';
    const TABLE_STATE = 'crm_states';
    const TABLE_STATUS_CUSTOMER = 'crm_status_customers';
    const TABLE_TOTAL_ACTIVE_DAY = 'crm_total_active_days';
    const TABLE_TYPE_BILLING = 'crm_type_billing_incomes';
    const TABLE_TYPE_CHARACTER = 'crm_type_character_incomes';
    const TABLE_TYPE_PACKET = 'crm_type_packets';
    const TABLE_TYPE_SERVICE = 'crm_type_services';
    const TABLE_TYPE_STATUS = 'crm_type_status_incomes';
    const TABLE_USER = 'users';


    const PUBLIC_PACKAGE = 'packages/beon/tools-income/';

    public static function navIsActive($search, $class_name)
    {
        $current_route = Route::getCurrentRoute()->getUri();
        return starts_with(str_replace(Config::get('syntara::config.uri').'/','',$current_route), $search)?$class_name:'';
    }

} 