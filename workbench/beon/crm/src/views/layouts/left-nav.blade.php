<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/29/14
 * Time: 10:02 PM
 */

use Beon\Crm\helpers\RouteHelper;
use Beon\Crm\helpers\StringHelper;


?>
@if($currentUser->hasAccess('hello-beon::hello-read-permission'))
<li class="treeview {{StringHelper::navIsActive(StringHelper::PACKAGE_NAME, 'active')}}" >
    <a href="#">
        <i class="fa fa-edit"></i> <span>Tools CRM</span>
        <i class="fa pull-right fa-angle-{{StringHelper::navIsActive(StringHelper::PACKAGE_NAME, 'active')?'down':'left'}}"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a style="margin-left: 10px;" href="{{RouteHelper::to('type-packets.manage')}}"><i class="fa fa-home"></i> Master Packet
                <i class="fa pull-right"></i></a>
        </li>
        <li>
            <a style="margin-left: 10px;" href="{{RouteHelper::to('type-services.manage')}}"><i class="fa fa-home"></i> Master Service
                <i class="fa pull-right"></i></a>
        </li>
    </ul>
</li>

@endif