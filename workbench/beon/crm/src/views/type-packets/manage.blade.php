<?php
use Beon\Crm\helpers\RouteHelper;
use Beon\Crm\helpers\StringHelper;

?>
@extends('ext-adminlte::layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12 content-box">
    </div>
</div>
@stop

@section('script:body-end')
<script   src="{{asset('packages/beon/js-admilte/js/lib/require/require.js')}}"></script>

<script id="table-tpl" type="text/html">
    <div class="box-body c-search">
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
        {{$modelName::tblTh()}}
    </tr>
    </thead>
        <tbody>

    </tbody>
    </table>
</script>

<script id="tr-tpl" type="text/html">
    {{$modelName::tblTd()}}
</script>


<script>

    var APP = {};
    APP.nameList='{{$nameList}}';
    APP.pageEnd = {{json_encode($modelName::lastPage())}};
    APP.formField = {{json_encode($modelName::$formFields)}};
    APP.formSearch = {{json_encode($modelName::searchConfig())}};
    APP.dataField = {{json_encode($modelName::formConfig())}};
    APP.friendlyNames = {{json_encode($modelName::$friendlyNames)}};

    APP.count = {{$modelName::count()}};

    APP.restUrl = '{{$restUrl}}';
    require(['{{ RouteHelper::asset('js/common.js') }}'], function (common) {
        require(['sform1/controller/crud']);
    });

</script>

@stop

