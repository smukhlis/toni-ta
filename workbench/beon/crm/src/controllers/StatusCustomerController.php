<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:02 PM
 */

namespace Beon\Crm\controllers;


use Beon\Crm\helpers\RouteHelper;
use Beon\Crm\helpers\StringHelper;
use Beon\JsAdminlte\controllers\BaseController;
use View;

class StatusCustomerController extends BaseController {

    public $className='Beon\Crm\models\StatusCustomer';

    public function manage()
    {
        $this->layout =  View::make('js-adminlte::layouts.crud', [
            'nameList'=>'Status Customer',
            'restUrl'=>RouteHelper::to("type-packets.index"),
            'modelName'=>$this->className
        ]);

        $this->layout->title = 'Jagoanweb - CRM - List Status Customer';

        // add breadcrumb to current page
        $this->layout->breadcrumb = array(
            array(
                'title' => 'Jagoanweb - Dashboard',
                'link' => 'dashboard',
                'icon' => 'glyphicon-home'
            ),
            array(
                'title' => 'List Status Customer',
                'link' =>  '#',
                'icon' => 'glyphicon glyphicon-th-list'
            ),
        );

        return $this->layout;
    }


}