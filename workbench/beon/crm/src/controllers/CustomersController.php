<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:02 PM
 */

namespace Beon\Crm\controllers;


use Beon\Crm\helpers\RouteHelper;
use Beon\Crm\helpers\StringHelper;
use Beon\JsAdminlte\controllers\BaseController;
use View;

class CustomersController extends BaseController {

    public $className='Beon\Crm\models\Customer';

    public function manage()
    {
        $this->layout =  View::make(StringHelper::PACKAGE_NAME . '::customers.manage', [
            'nameList'=>'Customers',
            'restUrl'=>RouteHelper::to("customers.index"),
            'modelName'=>$this->className
        ]);

        $this->layout->title = 'Jagoanweb - CRM - List Shared Potential Customer';

        // add breadcrumb to current page
        $this->layout->breadcrumb = array(
            array(
                'title' => 'Jagoanweb - Dashboard',
                'link' => 'dashboard',
                'icon' => 'glyphicon-home'
            ),
            array(
                'title' => 'List Shared Potential Customer',
                'link' =>  '#',
                'icon' => 'glyphicon glyphicon-th-list'
            ),
        );

        return $this->layout;
    }


}