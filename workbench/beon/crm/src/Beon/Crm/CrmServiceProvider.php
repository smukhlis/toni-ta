<?php namespace Beon\Crm;

use Illuminate\Support\ServiceProvider;

class CrmServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('beon/crm');
        $files_to_load = [
            'routes',
            'filters'
        ];

        foreach($files_to_load as $file)
        {
            if(file_exists($file = dirname(__FILE__).'/../../'.$file.'.php'))
                include $file;
        }
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
