<?php
/**
 * User: Saiful Mukhlis
 * Date: 8/28/14
 * Time: 10:02 PM
 */

namespace Beon\JsAdminlte\models;

use Input;
use Response;

abstract class  ModelJs extends \Eloquent implements iModelJs
{


    public static function tblTh()
    {
        $class = get_called_class();
        $out='<th class="th-sort" data-name="id" data-sort="asc"># <i class="fa fa-sort-asc"></i></th>';
        foreach($class::$tblItems as $th){
            $out .= '<th class="th-sort" data-name="'.$th.'"  data-sort="none">'.$class::label($th).'  <i class="fa fa-sort"></i></th>';
        }
        $out.='<th>Action </th>';
        return $out;
    }

    public static function tblTd()
    {
        $class = get_called_class();
        $out='<td><%=id%></td>';
        foreach($class::$tblItems as $th){
            $out .= '<td><%='.$th.'%></td>';
        }
        $out.='<td><button type="button" class="btn  btn-info btn-xs act-edit">Edit</button>  <button type="button" class="btn btn-danger btn-xs act-delete">Delete</button></td>';
        return $out;
    }

    public static  function label($name)
    {
        $class = get_called_class();
        $cn = $class::$friendlyNames;
        return $cn[$name];
    }

    public static function lastPage()
    {
        $class = get_called_class();
        $pageSize = Input::get('_page_size', 20);
        $sort = Input::get('_sort', 'id');
        $oder = Input::get('_order', 'asc');
        $count = $class::count();
        $countPage = ceil($count/$pageSize);
        return $class::take($pageSize)->skip(($countPage-1)*$pageSize)->orderBy($sort, $oder)->get();
    }

    public static function resultPage($result, $totalRow, $totalPage, $perPage, $currentPage)
    {
        return Response::json($result, 200, [
            'X-total'=>$totalRow,
            'X-page-size'=>$perPage,
            'X-page'=>$currentPage,
            'X-total-page'=>$totalPage,
        ]);
    }


    public static function page()
    {
        $class = get_called_class();
        $isEnd = Input::get('_end', false);
        $isSearch = Input::get('_search', false);
        $pageSize = Input::get('_page_size', 20);
        $sort = Input::get('_sort', 'id');
        $oder = Input::get('_order', 'asc');

        if($isSearch){
            $fields = $class::searchConfig();
            $search=false;
            $field=false;
            $index=0;
            $max = count($fields['data']);
            while($search==false){
                $search = Input::get($fields['data'][$index], false);
                if($search!=false){
                    $field=$fields['data'][$index];
                }
                $index++;
                if($index>=$max){
                    $search=true;
                }
            }
            if($field!=false){
                $count = $class::where($field,'like', '%' . $search . '%')->count();
                $countPage = ceil($count/$pageSize);
                if($isEnd){
                    return $class::resultPage(
                        self::where($field,'like', '%' . $search . '%')->take($pageSize)->skip(($countPage-1)*$pageSize)->orderBy($sort, $oder)->get(),
                        $count,
                        $countPage,
                        $pageSize,
                        $countPage
                    );
                }else{
                    $page = Input::get('_page');
                    return $class::resultPage(
                        self::where($field,'like', '%' . $search . '%')->take($pageSize)->skip(($page-1)*$pageSize)->orderBy($sort, $oder)->get(),
                        $count,
                        $countPage,
                        $pageSize,
                        $page
                    );
                }
            }


        }else{
            $count = self::count();
            $countPage = ceil($count/$pageSize);
            if($isEnd){
                return $class::resultPage(
                    self::take($pageSize)->skip(($countPage-1)*$pageSize)->orderBy($sort, $oder)->get(),
                    $count,
                    $countPage,
                    $pageSize,
                    $countPage);
            }else{
                $page = Input::get('_page');
                return $class::resultPage(
                    self::take($pageSize)->skip(($page-1)*$pageSize)->orderBy($sort, $oder)->get(),
                    $count,
                    $countPage,
                    $pageSize,
                    $page
                );
            }
        }

    }


}