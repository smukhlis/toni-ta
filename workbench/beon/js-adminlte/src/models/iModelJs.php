<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 12:47 AM
 */

namespace Beon\JsAdminlte\models;


interface iModelJs {
    public static function searchConfig();
    public static function formConfig();
    public static function tblTh();
    public static function tblTd();
    public static function label($name);
    public static function lastPage();
    public static function resultPage($result, $totalRow, $totalPage, $perPage, $currentPage);
    public static function page();
}