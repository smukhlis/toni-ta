<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:18 PM
 */

namespace Beon\JsAdminlte\helpers;

use Config;
use Route;

class StringHelper
{
    const VENDOR_NAME = 'beon';
    const PACKAGE_NAME = 'js-adminlte';
    const PUBLIC_PACKAGE = 'packages/beon/js-adminlte/';

    public static function navIsActive($search, $class_name)
    {
        $current_route = Route::getCurrentRoute()->getUri();
        return starts_with(str_replace(Config::get('syntara::config.uri').'/','',$current_route), $search)?$class_name:'';
    }

} 