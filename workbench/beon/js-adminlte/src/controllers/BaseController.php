<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:02 PM
 */

namespace Beon\JsAdminlte\controllers;

use Datatable;
use Input;
use MrJuliuss\Syntara\Controllers\BaseController as BC;
use Redirect;
use Request;
use Response;
use URL;
use Validator;
use View;

class BaseController extends BC {

    public $className;

    public function index()
    {
        //return call_user_func(array($className, 'page'));
        $className=$this->className;
        return $className::page();
    }

    /**
     * Show the form for creating a new resource.
     * GET /weblogos/create
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * POST /weblogos
     *
     * @return Response
     */
    public function store()
    {

        $className=$this->className;

        $data = Input::all();


        $validator = Validator::make($data, $className::$rules);
        $validator->setAttributeNames($className::$friendlyNames);
        if ($validator->fails())
        {
            return Response::json($validator->messages(), 422);
        }

        $logo = $className::create($data);

        if(Request::ajax())
        {
            return Response::json($logo);
        }

        //return Redirect::route(RouteHelper::to('logos.index'));
    }

    /**
     * Display the specified resource.
     * GET /weblogos/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $className=$this->className;
        return Response::json($className::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     * GET /weblogos/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     * PUT /weblogos/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $className=$this->className;
        $model = $className::findOrFail($id);


        $data = Input::all();

        $validator = Validator::make($data, $className::$rules);
        $validator->setAttributeNames($className::$friendlyNames);
        if ($validator->fails())
        {
            // return Redirect::back()->withErrors($validator)->withInput();

            return Response::json($validator->messages(), 422);
        }



        $model->update($data);


        if(Request::ajax())
        {

            return Response::json($model);
        }

        //return Redirect::route(RouteHelper::to('logos.index'));
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /weblogos/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $className=$this->className;
        $className::destroy($id);
        return $id;
    }
}