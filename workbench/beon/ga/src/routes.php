<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use Beon\Ga\helpers\RouteHelper;
use Beon\Ga\helpers\StringHelper;

Route::group([
    'before' => 'basicAuth',
    'prefix' => Config::get('syntara::config.uri').'/' . StringHelper::PACKAGE_NAME,
    'namespace'=> 'Beon\Ga\controllers'
], function()
{


    Route::get('test', 'TestController@index');


    Route::resource('companies', 'CompanyController');
    Route::get('company/manage', [
        'uses'=>'CompanyController@manage',
        'as'=>RouteHelper::baseName().'companies.manage'
    ]);

    Route::resource('engines', 'EngineController');
    Route::get('engine/manage', [
        'uses'=>'EngineController@manage',
        'as'=>RouteHelper::baseName().'engines.manage'
    ]);

    Route::resource('products', 'ProductController');
    Route::get('product/manage', [
        'uses'=>'ProductController@manage',
        'as'=>RouteHelper::baseName().'products.manage'
    ]);

    Route::resource('trxs', 'TrxController');
    Route::get('trx/manage', [
        'uses'=>'TrxController@manage',
        'as'=>RouteHelper::baseName().'trxs.manage'
    ]);

    Route::resource('schedules', 'ScheduleController');
    Route::get('schedule/manage', [
        'uses'=>'ScheduleController@manage',
        'as'=>RouteHelper::baseName().'schedules.manage'
    ]);


});
