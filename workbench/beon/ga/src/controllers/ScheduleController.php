<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:02 PM
 */

namespace Beon\Ga\controllers;


use Beon\Ga\helpers\RouteHelper;
use Beon\Ga\helpers\StringHelper;
use Beon\JsAdminlte\controllers\BaseController;
use View;

class ScheduleController extends BaseController {

    public $className='Beon\Ga\models\Schedule';

    public function manage()
    {
        $this->layout =  View::make(StringHelper::PACKAGE_NAME.'::schedules.manage', [
            'nameList'=>'Schedule',
            'restUrl'=>RouteHelper::to("schedules.index"),
            'modelName'=>$this->className
        ]);

        $this->layout->title = 'Jagoanweb - List Schedule';

        // add breadcrumb to current page
        $this->layout->breadcrumb = array(
            array(
                'title' => 'Jagoanweb - Dashboard',
                'link' => 'dashboard',
                'icon' => 'glyphicon-home'
            ),
            array(
                'title' => 'List Schedule',
                'link' =>  '#',
                'icon' => 'glyphicon glyphicon-th-list'
            ),
        );

        return $this->layout;
    }


}