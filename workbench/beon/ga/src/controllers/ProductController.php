<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:02 PM
 */

namespace Beon\Ga\controllers;


use Beon\Ga\helpers\RouteHelper;
use Beon\Ga\helpers\StringHelper;
use Beon\Ga\models\Product;
use Beon\JsAdminlte\controllers\BaseController;
use Input;
use Request;
use Response;
use Validator;
use View;

class ProductController extends BaseController {

    public $className='Beon\Ga\models\Product';

    public function manage()
    {
        $this->layout =  View::make(StringHelper::PACKAGE_NAME.'::layouts.crud', [
            'nameList'=>'Product',
            'restUrl'=>RouteHelper::to("products.index"),
            'modelName'=>$this->className
        ]);

        $this->layout->title = 'Jagoanweb - List Product';

        // add breadcrumb to current page
        $this->layout->breadcrumb = array(
            array(
                'title' => 'Jagoanweb - Dashboard',
                'link' => 'dashboard',
                'icon' => 'glyphicon-home'
            ),
            array(
                'title' => 'List Product',
                'link' =>  '#',
                'icon' => 'glyphicon glyphicon-th-list'
            ),
        );

        return $this->layout;
    }


    public function store()
    {

        $className=$this->className;

        $data = Input::all();

        $engines = Product::engines();
        $arr=[];
        foreach ($engines as $engine) {
            $arr[$engine->id]=Input::get('engine'.$engine->id, 0);
        }
        $data['schedule']=json_encode($arr);

        $validator = Validator::make($data, $className::$rules);
        $validator->setAttributeNames($className::$friendlyNames);
        if ($validator->fails())
        {
            return Response::json($validator->messages(), 422);
        }

        $logo = $className::create($data);

        if(Request::ajax())
        {
            return Response::json($logo);
        }

        //return Redirect::route(RouteHelper::to('logos.index'));
    }

    public function update($id)
    {
        $className=$this->className;
        $model = $className::findOrFail($id);


        $data = Input::all();
        $engines = Product::engines();
        $arr=[];
        foreach ($engines as $engine) {
            $arr[$engine->id]=Input::get('engine'.$engine->id, 0);
        }
        $data['schedule']=json_encode($arr);

        $validator = Validator::make($data, $className::$rules);
        $validator->setAttributeNames($className::$friendlyNames);
        if ($validator->fails())
        {
            // return Redirect::back()->withErrors($validator)->withInput();

            return Response::json($validator->messages(), 422);
        }



        $model->update($data);


        if(Request::ajax())
        {

            return Response::json($model);
        }

        //return Redirect::route(RouteHelper::to('logos.index'));
    }


}