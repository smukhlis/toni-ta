<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:02 PM
 */

namespace Beon\Ga\controllers;


use Beon\Ga\helpers\RouteHelper;
use Beon\Ga\helpers\StringHelper;
use Beon\JsAdminlte\controllers\BaseController;
use View;

class TrxController extends BaseController {

    public $className='Beon\Ga\models\Trx';

    public function manage()
    {
        $this->layout =  View::make('js-adminlte::layouts.crud', [
            'nameList'=>'Order',
            'restUrl'=>RouteHelper::to("trxs.index"),
            'modelName'=>$this->className
        ]);

        $this->layout->title = 'Jagoanweb - List Order';

        // add breadcrumb to current page
        $this->layout->breadcrumb = array(
            array(
                'title' => 'Jagoanweb - Dashboard',
                'link' => 'dashboard',
                'icon' => 'glyphicon-home'
            ),
            array(
                'title' => 'List Trx',
                'link' =>  '#',
                'icon' => 'glyphicon glyphicon-th-list'
            ),
        );

        return $this->layout;
    }


}