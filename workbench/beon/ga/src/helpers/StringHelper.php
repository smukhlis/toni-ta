<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:18 PM
 */

namespace Beon\Ga\helpers;

use Config;
use Route;

class StringHelper
{
    const VENDOR_NAME = 'beon';
    const PACKAGE_NAME = 'ga';

    const TABLE_COMPANY = 'companies';
    const TABLE_ENGINE = 'engines';
    const TABLE_PRUDUCT = 'pruducts';
    const TABLE_TRX = 'trxs';
    const TABLE_USER = 'users';


    const PUBLIC_PACKAGE = 'packages/beon/ga/';

    public static function navIsActive($search, $class_name)
    {
        $current_route = Route::getCurrentRoute()->getUri();
        return starts_with(str_replace(Config::get('syntara::config.uri').'/','',$current_route), $search)?$class_name:'';
    }

} 