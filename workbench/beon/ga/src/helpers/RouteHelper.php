<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/28/14
 * Time: 10:18 PM
 */

namespace Beon\Ga\helpers;

use Config;

use Illuminate\Support\Facades\Route;

class RouteHelper
{
    public static function to($name, $param=null)
    {
        if($param!=null)return route(Config::get('syntara::config.uri') . '.' . StringHelper::PACKAGE_NAME . '.' . $name, $param);
        return route(Config::get('syntara::config.uri') . '.' . StringHelper::PACKAGE_NAME . '.' . $name);
    }

    public static function baseName()
    {
        return Config::get('syntara::config.uri') . '.' . StringHelper::PACKAGE_NAME . '.';
    }

    public static function asset($name)
    {
        return asset('packages/' . StringHelper::VENDOR_NAME . '/' .StringHelper::PACKAGE_NAME . '/' . $name);
    }
} 