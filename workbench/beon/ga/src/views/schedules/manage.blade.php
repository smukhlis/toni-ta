<?php
use Beon\JsAdminlte\helpers\RouteHelper;
use Beon\JsAdminlte\helpers\StringHelper;

?>
@extends('ext-adminlte::layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12 content-box">
    </div>
</div>
@stop

@section('script:body-end')
<script   src="{{ RouteHelper::asset('js/lib/require/require.js') }}"></script>




<script>

    var APP = {};
    APP.nameList='{{$nameList}}';
    APP.formField = {{json_encode($modelName::$formFields)}};
    APP.dataField = {{json_encode($modelName::formConfig())}};
    APP.friendlyNames = {{json_encode($modelName::$friendlyNames)}};


    APP.restUrl = '{{$restUrl}}';
    require(['{{ RouteHelper::asset('js/common.js') }}'], function (common) {
        require(['sga1/controller/manage-schedule']);
    });

</script>

@stop

