<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 8/29/14
 * Time: 10:02 PM
 */

use Beon\Ga\helpers\RouteHelper;
use Beon\Ga\helpers\StringHelper;


?>
@if($currentUser->hasAccess('hello-beon::hello-read-permission'))
<li class="treeview {{StringHelper::navIsActive(StringHelper::PACKAGE_NAME, 'active')}}" >
    <a href="{{RouteHelper::to('companies.manage')}}">
        <i class="fa fa-edit"></i> <span>Company</span>
    </a>
    <a href="{{RouteHelper::to('engines.manage')}}">
        <i class="fa fa-edit"></i> <span>Engine</span>
    </a>
    <a href="{{RouteHelper::to('products.manage')}}">
        <i class="fa fa-edit"></i> <span>Product</span>
    </a>
    <a href="{{RouteHelper::to('trxs.manage')}}">
        <i class="fa fa-edit"></i> <span>Order</span>
    </a>
    <a href="{{RouteHelper::to('schedules.manage')}}">
        <i class="fa fa-edit"></i> <span>Schedule</span>
    </a>

</li>

@endif