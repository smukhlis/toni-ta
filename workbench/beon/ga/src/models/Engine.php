<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Ga\models;


use Beon\Ga\helpers\StringHelper;
use Beon\JsAdminlte\models\ModelJs;

class Engine extends ModelJs{

    protected $table = StringHelper::TABLE_ENGINE;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'total',
        'sort',
    ];

    public static $formFields = [
        'name',
        'description',
        'total',
        'sort',
    ];

    public static $tblItems = [
        'name',
        'description',
        'total',
        'sort',
    ];

    public static  $friendlyNames =[
        'id'=>'Id',
        'name'=>'Name',
        'description'=>'Description',
        'total'=>'Total',
        'sort'=>'Sort',
        'created_at'=>'Created at',
        'updated_at'=>'Updated at',
    ];

    public static $rules = [
        'name'=>'required|max:255',
        'cp'=>'max:255',
        'description'=>'max:255',
        'total'=>'numeric',
        'sort'=>'numeric'
    ];


    public static function searchConfig()
    {
        return [
            'default'=>'name',
            'data'=>[
                'id',
                'name',
                'description',
                'total',
                'sort',
            ]
        ];
    }

    public static function formConfig()
    {
        return [
            'sort'=>[
                'field'=>'textfield'
            ]
        ];
    }
}