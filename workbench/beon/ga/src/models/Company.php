<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Ga\models;


use Beon\Ga\helpers\StringHelper;
use Beon\JsAdminlte\models\ModelJs;

class Company extends ModelJs{

    protected $table = StringHelper::TABLE_COMPANY;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'name',
        'cp',
        'telp',
        'address',
    ];

    public static $formFields = [
        'name',
        'cp',
        'telp',
        'address',
    ];

    public static $tblItems = [
        'name',
        'cp',
        'telp',
        'address',
    ];

    public static  $friendlyNames =[
        'id'=>'Id',
        'name'=>'Name',
        'cp'=>'Contact Person',
        'telp'=>'Telp',
        'address'=>'Address',
        'created_at'=>'Created at',
        'updated_at'=>'Updated at',
    ];

    public static $rules = [
        'name'=>'required|max:255',
        'cp'=>'max:255',
        'telp'=>'max:255',
        'address'=>'max:255'
    ];


    public static function searchConfig()
    {
        return [
            'default'=>'name',
            'data'=>[
                'id',
                'name',
                'cp',
                'telp',
                'address',
            ]
        ];
    }

    public static function formConfig()
    {
        return [
            'address'=>[
                'field'=>'textarea'
            ]
        ];
    }
}