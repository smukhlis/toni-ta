<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Ga\models;


use Beon\Ga\helpers\StringHelper;
use Beon\JsAdminlte\models\ModelJs;
use Input;
use Response;

class Product extends ModelJs{

    protected $table = StringHelper::TABLE_PRUDUCT;

    public static $_engines;
    public static function engines()
    {
        if(self::$_engines==null){
            self::$_engines=Engine::orderBy('sort')->get();
        }
        return self::$_engines;
    }

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'name',
        'schedule',
    ];

    public static $formFields = [
        'name',
        'schedule',
    ];

    public static function  formFields()
    {
        $arr = ['name'];
        $engines = self::engines();
        foreach ($engines as $engine) {
            $arr[]='engine'.$engine->id;
        }

        return $arr;
    }

    public static $tblItems = [
        'name',
        'schedule',
    ];

    public static function  tblItems()
    {
        $arr = ['name'];
        $engines = self::engines();
        foreach ($engines as $engine) {
            $arr[]='engine'.$engine->id;
        }

        return $arr;
    }

    public static function tblTh()
    {
        $class = get_called_class();
        $out='<th class="th-sort" data-name="id" data-sort="asc"># <i class="fa fa-sort-asc"></i></th>';
        foreach($class::tblItems() as $th){
            $out .= '<th class="th-sort" data-name="'.$th.'"  data-sort="none">'.$class::label($th).'  <i class="fa fa-sort"></i></th>';
        }
        $out.='<th>Action </th>';
        return $out;
    }

    public static function tblTd()
    {
        $class = get_called_class();
        $out='<td><%=id%></td>';
        foreach($class::tblItems() as $th){
            $out .= '<td><%='.$th.'%></td>';
        }
        $out.='<td><button type="button" class="btn  btn-info btn-xs act-edit">Edit</button>  <button type="button" class="btn btn-danger btn-xs act-delete">Delete</button></td>';
        return $out;
    }

    public static  $friendlyNames =[
        'id'=>'Id',
        'name'=>'Name',
        'schedule'=>'Schedule',
        'created_at'=>'Created at',
        'updated_at'=>'Updated at',
    ];

    public static function  friendlyNames()
    {
        $arr = self::$friendlyNames;
        $engines = self::engines();
        foreach ($engines as $engine) {
            $arr['engine'.$engine->id]=$engine->name;
        }

        return $arr;
    }

    public static  function label($name)
    {
        $class = get_called_class();
        $cn = $class::friendlyNames();
        return $cn[$name];
    }

    public static $rules = [
        'name'=>'required|max:255'
    ];


    public static function searchConfig()
    {
        return [
            'default'=>'name',
            'data'=>[
                'id',
                'name',
            ]
        ];
    }

    public static function formConfig()
    {
        return [];
    }

    public static function resultPage($result, $totalRow, $totalPage, $perPage, $currentPage)
    {

        //$result = parent::page();
        foreach ($result as $item) {
            $item->build();
        }
        return Response::json($result, 200, [
            'X-total'=>$totalRow,
            'X-page-size'=>$perPage,
            'X-page'=>$currentPage,
            'X-total-page'=>$totalPage,
        ]);
    }

    public function build()
    {
        $engines = self::engines();
        $scd = json_decode($this->schedule, true);
        foreach ($engines as $engine) {
            $this->attributes['engine'.$engine->id]=$scd[$engine->id];
            $this->original['engine'.$engine->id]=$scd[$engine->id];
        }


    }

    public static function lastPage()
    {
        $class = get_called_class();
        $pageSize = Input::get('_page_size', 20);
        $sort = Input::get('_sort', 'id');
        $oder = Input::get('_order', 'asc');
        $count = $class::count();
        $countPage = ceil($count/$pageSize);
        $result = $class::take($pageSize)->skip(($countPage-1)*$pageSize)->orderBy($sort, $oder)->get();
        foreach ($result as $item) {
            $item->build();
        }
        return $result;
    }

//    public static function page()
//    {
//        $engines = self::engines();
//        $result = parent::page();
//        foreach ($result as $item) {
//            echo '<pre>';
//            print_r($item);
//            echo '</pre>';
//        }
//        exit;
//
//    }
}