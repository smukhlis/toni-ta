<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Ga\models;


use Beon\Ga\helpers\StringHelper;
use Beon\JsAdminlte\models\ModelJs;

class Schedule extends ModelJs{

    //protected $table = StringHelper::TABLE_COMPANY;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'name',
        'cp',
        'telp',
        'address',
    ];

    public static $formFields = [
        'start_population',
        'crosover',
        'mutasi',
        'selection',
    ];

    public static $tblItems = [
        'name',
        'cp',
        'telp',
        'address',
    ];

    public static  $friendlyNames =[
        'start_population'=>'Start Population',
        'crosover'=>'Crosover(%)',
        'mutasi'=>'Mutasi(%)',
        'selection'=>'Selection(%)'
    ];

    public static $rules = [
        'start_population'=>'required|min:1|numeric',
        'crosover'=>'required|min:1|numeric',
        'mutasi'=>'required|min:1|numeric',
        'selection'=>'required|min:1|numeric'
    ];


    public static function searchConfig()
    {
        return [
            'default'=>'name',
            'data'=>[
                'id',
                'name',
                'cp',
                'telp',
                'address',
            ]
        ];
    }

    public static function formConfig()
    {
        return [
            'address'=>[
                'field'=>'textarea'
            ]
        ];
    }
}