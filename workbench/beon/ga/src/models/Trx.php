<?php
/**
 * Created by PhpStorm.
 * User: sai
 * Date: 9/19/14
 * Time: 1:26 AM
 */

namespace Beon\Ga\models;


use Beon\Ga\helpers\StringHelper;
use Beon\JsAdminlte\models\ModelJs;
use Input;

class Trx extends ModelJs{

    protected $table = StringHelper::TABLE_TRX;

    /**
     * field yang boleh di manipulasi ke db
     * @var array
     */
    protected $fillable = [
        'product_id',
        'company_id',
        'count',
        'status',
    ];

    public static $formFields = [
        'product_id',
        'company_id',
        'count',
        //'status',
    ];

    public static $tblItems = [
        ['product'=>'name'],
        ['company'=>'name'],
        'count',
        //'status',
    ];

    public static  $friendlyNames =[
        'id'=>'Id',
        'product_id'=>'Product',
        'company_id'=>'Company',
        'product.name'=>'Nama Product',
        'company.name'=>'Nama Company',
        'count'=>'Count',
        'status'=>'Status',
        'created_at'=>'Created at',
        'updated_at'=>'Updated at',
    ];

    public static $rules = [
        'product_id'=>'required|numeric',
        'company_id'=>'numeric',
        'count'=>'numeric',
        'status'=>'numeric'
    ];


    public static function searchConfig()
    {
        return [
            'default'=>'id',
            'data'=>[
                'id',
                'product_id',
                'company_id',
                'count',
                //'status',
            ]
        ];
    }

    public static function formConfig()
    {
        return [
            'product_id'=>[
                'field'=>'combobox',
                'dataList'=>Product::orderBy('name')->lists('name', 'id')
            ],
            'company_id'=>[
                'field'=>'combobox',
                'dataList'=>Company::orderBy('name')->lists('name', 'id')
            ]
        ];
    }

    public function company()
    {
        return $this->belongsTo('Beon\Ga\models\Company', 'company_id');
    }

    public function product()
    {
        return $this->belongsTo('Beon\Ga\models\Product', 'product_id');
    }



    public static function page()
    {
        $class = get_called_class();
        $isEnd = Input::get('_end', false);
        $isSearch = Input::get('_search', false);
        $pageSize = Input::get('_page_size', 20);
        $sort = Input::get('_sort', 'id');
        $oder = Input::get('_order', 'asc');

        if($isSearch){
            $fields = $class::searchConfig();
            $search=false;
            $field=false;
            $index=0;
            $max = count($fields['data']);
            while($search==false){
                $search = Input::get($fields['data'][$index], false);
                if($search!=false){
                    $field=$fields['data'][$index];
                }
                $index++;
                if($index>=$max){
                    $search=true;
                }
            }
            if($field!=false){
                $count = $class::where($field,'like', '%' . $search . '%')->count();
                $countPage = ceil($count/$pageSize);
                if($isEnd){
                    return $class::resultPage(
                        self::rel()->where($field,'like', '%' . $search . '%')->take($pageSize)->skip(($countPage-1)*$pageSize)->orderBy($sort, $oder)->get(),
                        $count,
                        $countPage,
                        $pageSize,
                        $countPage
                    );
                }else{
                    $page = Input::get('_page');
                    return $class::resultPage(
                        self::rel()->where($field,'like', '%' . $search . '%')->take($pageSize)->skip(($page-1)*$pageSize)->orderBy($sort, $oder)->get(),
                        $count,
                        $countPage,
                        $pageSize,
                        $page
                    );
                }
            }


        }else{
            $count = self::count();
            $countPage = ceil($count/$pageSize);
            if($isEnd){
                return $class::resultPage(
                    self::rel()->take($pageSize)->skip(($countPage-1)*$pageSize)->orderBy($sort, $oder)->get(),
                    $count,
                    $countPage,
                    $pageSize,
                    $countPage);
            }else{
                $page = Input::get('_page');
                return $class::resultPage(
                    self::rel()->take($pageSize)->skip(($page-1)*$pageSize)->orderBy($sort, $oder)->get(),
                    $count,
                    $countPage,
                    $pageSize,
                    $page
                );
            }
        }

    }

    public static function lastPage()
    {
        $class = get_called_class();
        $pageSize = Input::get('_page_size', 20);
        $sort = Input::get('_sort', 'id');
        $oder = Input::get('_order', 'asc');
        $count = $class::count();
        $countPage = ceil($count/$pageSize);
        return $class::rel()->take($pageSize)->skip(($countPage-1)*$pageSize)->orderBy($sort, $oder)->get();
    }

    public static function  rel()
    {
        $class = get_called_class();
        return $class::with('company','product');
    }

    public static function tblTh()
    {
        $class = get_called_class();
        $out='<th class="th-sort" data-name="id" data-sort="asc"># <i class="fa fa-sort-asc"></i></th>';
        foreach($class::$tblItems as $th){
            if(is_array($th)){
                foreach($th as $k=>$v){
                    $out .= '<th class="th-sort" data-name="'.$k.'.'.$v.'"  data-sort="none">'.$class::label($k.'.'.$v).'  <i class="fa fa-sort"></i></th>';
                }
            }else{
                $out .= '<th class="th-sort" data-name="'.$th.'"  data-sort="none">'.$class::label($th).'  <i class="fa fa-sort"></i></th>';
            }

        }
        $out.='<th>Action </th>';
        return $out;
    }

    public static function tblTd()
    {
        $class = get_called_class();
        $out='<td><%=id%></td>';
        foreach($class::$tblItems as $th){
            if(is_array($th)){
                foreach($th as $k=>$v){
                    $out .= '<td><%='.$k.'["'.$v.'"]%></td>';
                }
            }else{
                $out .= '<td><%='.$th.'%></td>';
            }

        }
        $out.='<td><button type="button" class="btn  btn-info btn-xs act-edit">Edit</button>  <button type="button" class="btn btn-danger btn-xs act-delete">Delete</button></td>';
        return $out;
    }
}