<?php
use Beon\Ga\helpers\StringHelper;

View::composer('ext-adminlte::layouts.master', function($view)
{

    $view->with('navPages', View::make(StringHelper::PACKAGE_NAME . '::layouts.left-nav').$view->navPages);

});

View::composer('adminlte::layouts.dashboard.master', function($view)
{
    $view->with('navPages', View::make(StringHelper::PACKAGE_NAME . '::layouts.left-nav').$view->navPages);
});