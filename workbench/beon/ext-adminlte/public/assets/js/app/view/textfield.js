/**
 *
 */
define([
    'jquery',
    'underscore',
    'marionette',
    "text!app/template/textfield.html"
], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        defaults: {
            label: 'Label',
            name: 'name'
        },
        template : _.template(template),
        className : 'col-lg-6',

        initialize: function () {
            this.options = _.extend(this.defaults, this.options);
            _.bindAll(this, 'onRender');
        },
        onRender: function () {

        },
        serializeData: function(){
            var data = {};
            data.label = this.options.label;
            data.name = this.options.name;
            return data;
        }

    });

});