

CREATE TABLE IF NOT EXISTS `crm_incomes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_date` timestamp NULL DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `invoice_total` decimal(12,2) DEFAULT NULL,
  `customer_id` int(10)  unsigned NOT NULL,
  `user_id` int(10)  unsigned NOT NULL,
  `section_id` int(10)  unsigned NOT NULL,
  `type_service_id` int(10)  unsigned NOT NULL,
  `type_packet_id` int(10)  unsigned NOT NULL,
  `type_billing_id` int(10)  unsigned NOT NULL,
  `status_invoice` int(10)  unsigned NOT NULL,
  `status_validate` int(10)  unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `type_service_id` (`type_service_id`),
  KEY `type_packet_id` (`type_packet_id`),
  KEY `type_billing_id` (`type_billing_id`),
  KEY `status_invoice` (`status_invoice`),
  KEY `status_validate` (`status_validate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `crm_sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `source` varchar(255)  DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `target_income` decimal(12,2) DEFAULT NULL,
  `default_setting` text,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `source` (`source`),
  KEY `target_income` (`target_income`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `crm_section_mutations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sections_id` int(10) unsigned DEFAULT NULL,
  `total_active` int(11) DEFAULT NULL,
  `total_waiting` int(11) DEFAULT NULL,
  `total_inactive` int(11) DEFAULT NULL,
  `total_lost` int(11) DEFAULT NULL,
  `total_income`  decimal(12,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_payment_sugestions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_date` timestamp NULL DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `invoice_total` decimal(12,2) DEFAULT NULL,
  `customer_id` int(10)  unsigned NOT NULL,
  `type_service_id` int(10)  unsigned NOT NULL,
  `type_packet_id` int(10)  unsigned NOT NULL,
  `type_billing_id` int(10)  unsigned NOT NULL,
  `status_invoice` int(10)  unsigned NOT NULL,
  `status_validate` int(10)  unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `type_service_id` (`type_service_id`),
  KEY `type_packet_id` (`type_packet_id`),
  KEY `type_billing_id` (`type_billing_id`),
  KEY `status_invoice` (`status_invoice`),
  KEY `status_validate` (`status_validate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_followups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10)  unsigned NOT NULL,
  `section_id` int(10)  unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type_followup_id` int(10)  unsigned NOT NULL,
  `total_target`  decimal(12,2) DEFAULT NULL,
  `conv_rate`  decimal(12,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `section_id` (`section_id`),
  KEY `name` (`name`),
  KEY `type_followup_id` (`type_followup_id`),
  KEY `total_target` (`total_target`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `date_of_birth`  timestamp NULL DEFAULT NULL,
  `place_of_birth` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `type_service_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE `email` (`email`),
  UNIQUE `phone` (`phone`),
  KEY `source` (`source`),
  KEY `name` (`name`),
  KEY `domain` (`domain`),
  KEY `city` (`city`),
  KEY `type_service_id` (`type_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_customer_leads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10)  unsigned NOT NULL,
  `type_character_id` int(10)  unsigned NOT NULL,
  `target_type_service_id` int(10)  unsigned NOT NULL,
  `target_type_packet_id` int(10)  unsigned NOT NULL,
  `target_type_billing_id` int(10)  unsigned NOT NULL,
  `target_amount` int(10)  unsigned NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `is_new` int(10)  unsigned NOT NULL,
  `status` int(10)  unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `type_character_id` (`type_character_id`),
  KEY `target_type_service_id` (`target_type_service_id`),
  KEY `target_type_packet_id` (`target_type_packet_id`),
  KEY `target_type_billing_id` (`target_type_billing_id`),
  KEY `target_amount` (`target_amount`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `crm_type_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `crm_type_packets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_type_billing_incomes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_type_status_incomes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `crm_type_character_incomes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_status_customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `crm_sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10)  unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `name` (`name`),
  KEY `user_id` (`user_id`),
  KEY `path` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_total_active_days` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` int(10)  unsigned NOT NULL,
  `year` int(10)  unsigned NOT NULL,
  `active_day` int(10)  unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `month` (`month`),
  KEY `year` (`year`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_app_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `data` text,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `crm_app_caches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  unique `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







